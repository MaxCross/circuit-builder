﻿using System;
using System.Data.SqlTypes;
using System.IO;

namespace OrbitalShipHacker.VM
{
	public class MCAVirtualMachine
	{
		public ushort DataLength => (ushort) Data.Length;
		public readonly byte[] Data;

		public ushort InstructionPointer
		{
			get => BitConverter.ToUInt16(Data, 0);
			set => BitConverter.GetBytes(value).CopyTo(Data, 0);
		}

		public MCAVirtualMachine(ushort dataLength = 0xffff)
		{
			Data = new byte[dataLength];
			InstructionPointer = 16;
		}

		public MCAVirtualMachine(byte[] data) => Data = data;

		public byte GetByte(ushort index) => Data[index % DataLength];
		public void SetByte(ushort index, byte value) => Data[index % DataLength] = value;
		public ushort GetUInt16(ushort index) => BitConverter.ToUInt16(Data, index % DataLength);
		public void SetUInt16(ushort index, ushort value) => BitConverter.GetBytes(value).CopyTo(Data, index % DataLength);
		
		public enum Instructions : byte
		{
			nop      = 0x00,
			not_b    = 0x00 + 0b0010,
			not_s    = 0x0 + 0b0011,
			
			mov_vb  = 0x00 + 0b01_00,
			mov_vs  = 0x00 + 0b01_01,
			mov_ab  = 0x00 + 0b01_10,
			mov_as  = 0x00 + 0b01_11,
			
			
			add_vb  = 0x10 + 0b00_00,
			add_vs  = 0x10 + 0b00_01,
			add_ab  = 0x10 + 0b00_10,
			add_as  = 0x10 + 0b00_11,
			
			sub_vb  = 0x10 + 0b01_00,
			sub_vs  = 0x10 + 0b01_01,
			sub_ab  = 0x10 + 0b01_10,
			sub_as  = 0x10 + 0b01_11,
			
			mul_vb  = 0x10 + 0b10_00,
			mul_vs  = 0x10 + 0b10_01,
			mul_ab  = 0x10 + 0b10_10,
			mul_as  = 0x10 + 0b10_11,
			
			mod_vb  = 0x10 + 0b11_00,
			mod_vs  = 0x10 + 0b11_01,
			mod_ab  = 0x10 + 0b11_10,
			mod_as  = 0x10 + 0b11_11,
			
			or_vb   = 0x20 + 0b00_00,
			or_vs   = 0x20 + 0b00_01,
			or_ab   = 0x20 + 0b00_10,
			or_as   = 0x20 + 0b00_11,
			
			xor_vb  = 0x20 + 0b01_00,
			xor_vs  = 0x20 + 0b01_01,
			xor_ab  = 0x20 + 0b01_10,
			xor_as  = 0x20 + 0b01_11,
			
			and_vb  = 0x20 + 0b10_00,
			and_vs  = 0x20 + 0b10_01,
			and_ab  = 0x20 + 0b10_10,
			and_as  = 0x20 + 0b10_11,
			
			//TODO:Вставить что-нибудь на 0x2 + 0b11xx
			
			
			shl_vb  = 0x30 + 0b00_00,
			shl_vs  = 0x30 + 0b00_01,
			shl_ab  = 0x30 + 0b00_10,
			shl_as  = 0x30 + 0b00_11,
			
			shr_vb  = 0x30 + 0b01_00,
			shr_vs  = 0x30 + 0b01_01,
			shr_ab  = 0x30 + 0b01_10,
			shr_as  = 0x30 + 0b01_11,
			
			//TODO:Вставить что-нибудь на 0x3 + 0b10xx и 0x3 + 0b10xx
			
			jl_vb   = 0x40 + 0b00_00,
			jl_vs   = 0x40 + 0b00_01,
			jl_ab   = 0x40 + 0b00_10,
			jl_as   = 0x40 + 0b00_11,
			
			jg_vb   = 0x40 + 0b01_00,
			jg_vs   = 0x40 + 0b01_01,
			jg_ab   = 0x40 + 0b01_10,
			jg_as   = 0x40 + 0b01_11,
			
			je_vb   = 0x40 + 0b10_00,
			je_vs   = 0x40 + 0b10_01,
			je_ab   = 0x40 + 0b10_10,
			je_as   = 0x40 + 0b10_11,
			
			jne_vb  = 0x40 + 0b11_00,
			jne_vs  = 0x40 + 0b11_01,
			jne_ab  = 0x40 + 0b11_10,
			jne_as  = 0x40 + 0b11_11,
			
			Unknown = 0xff
		}


		public void ExecuteCurrentInstruction()
		{
			var prevIp = InstructionPointer;
			var instruction = (Instructions) GetByte(InstructionPointer);
			var adr0 = GetUInt16((ushort) (InstructionPointer + 1));
			var adr1 = GetUInt16((ushort) (InstructionPointer + 1 + 2));

			var val1 = GetByte((ushort) (InstructionPointer + 1 + 2));
			var val1s = adr1;

			switch (instruction)
			{
				case Instructions.nop:
					if(InstructionPointer == prevIp) InstructionPointer++;
					break;
				
				case Instructions.not_b:
					SetByte(adr0, (byte) ~GetByte(adr0));
					if(InstructionPointer == prevIp) InstructionPointer += 3;
					break;
				case Instructions.not_s:
					SetUInt16(adr0, (ushort) ~GetUInt16(adr0));
					if(InstructionPointer == prevIp) InstructionPointer += 3;
					break;
				
				case Instructions.mov_vb:
					SetByte(adr0, val1);
					if(InstructionPointer == prevIp) InstructionPointer += 4;
					break;
				case Instructions.mov_vs:
					SetUInt16(adr0, val1s);
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				case Instructions.mov_ab:
					SetByte(adr0, GetByte(adr1));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				case Instructions.mov_as:
					SetUInt16(adr0, GetUInt16(adr1));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				
				
				case Instructions.add_vb:
					SetByte(adr0, (byte) (GetByte(adr0) + val1));
					if(InstructionPointer == prevIp) InstructionPointer += 4;
					break;
				case Instructions.add_vs:
					SetUInt16(adr0, (ushort) (GetUInt16(adr0) + val1s));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				case Instructions.add_ab:
					SetByte(adr0, (byte) (GetByte(adr0) + GetByte(adr1)));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				case Instructions.add_as:
					SetUInt16(adr0, (ushort) (GetUInt16(adr0) + GetUInt16(adr1)));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				
				case Instructions.sub_vb:
					SetByte(adr0, (byte) (GetByte(adr0) - val1));
					if(InstructionPointer == prevIp) InstructionPointer += 4;
					break;
				case Instructions.sub_vs:
					SetUInt16(adr0, (ushort) (GetUInt16(adr0) - val1s));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				case Instructions.sub_ab:
					SetByte(adr0, (byte) (GetByte(adr0) - GetByte(adr1)));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				case Instructions.sub_as:
					SetUInt16(adr0, (ushort) (GetUInt16(adr0) - GetUInt16(adr1)));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				
				case Instructions.mul_vb:
					SetByte(adr0, (byte) (GetByte(adr0) * val1));
					if(InstructionPointer == prevIp) InstructionPointer += 4;
					break;
				case Instructions.mul_vs:
					SetUInt16(adr0, (ushort) (GetUInt16(adr0) * val1s));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				case Instructions.mul_ab:
					SetByte(adr0, (byte) (GetByte(adr0) * GetByte(adr1)));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				case Instructions.mul_as:
					SetUInt16(adr0, (ushort) (GetUInt16(adr0) * GetUInt16(adr1)));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				
				case Instructions.mod_vb:
					SetByte(adr0, (byte) (GetByte(adr0) % val1));
					if(InstructionPointer == prevIp) InstructionPointer += 4;
					break;
				case Instructions.mod_vs:
					SetUInt16(adr0, (ushort) (GetUInt16(adr0) % val1s));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				case Instructions.mod_ab:
					if(GetByte(adr1) != 0) SetByte(adr0, (byte) (GetByte(adr0) % GetByte(adr1)));
					else SetByte(adr0, 0);
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				case Instructions.mod_as:
					if(GetUInt16(adr1) != 0) SetUInt16(adr0, (ushort) (GetUInt16(adr0) % GetUInt16(adr1)));
					else SetUInt16(adr0, 0);
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				
				
				case Instructions.or_vb:
					SetByte(adr0, (byte) (GetByte(adr0) | val1));
					if(InstructionPointer == prevIp) InstructionPointer += 4;
					break;
				case Instructions.or_vs:
					SetUInt16(adr0, (ushort) (GetUInt16(adr0) | val1s));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				case Instructions.or_ab:
					SetByte(adr0, (byte) (GetByte(adr0) | GetByte(adr1)));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				case Instructions.or_as:
					SetUInt16(adr0, (ushort) (GetUInt16(adr0) | GetUInt16(adr1)));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				
				case Instructions.xor_vb:
					SetByte(adr0, (byte) (GetByte(adr0) ^ val1));
					if(InstructionPointer == prevIp) InstructionPointer += 4;
					break;
				case Instructions.xor_vs:
					SetUInt16(adr0, (ushort) (GetUInt16(adr0) ^ val1s));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				case Instructions.xor_ab:
					SetByte(adr0, (byte) (GetByte(adr0) ^ GetByte(adr1)));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				case Instructions.xor_as:
					SetUInt16(adr0, (ushort) (GetUInt16(adr0) ^ GetUInt16(adr1)));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				
				case Instructions.and_vb:
					SetByte(adr0, (byte) (GetByte(adr0) & val1));
					if(InstructionPointer == prevIp) InstructionPointer += 4;
					break;
				case Instructions.and_vs:
					SetUInt16(adr0, (ushort) (GetUInt16(adr0) & val1s));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				case Instructions.and_ab:
					SetByte(adr0, (byte) (GetByte(adr0) & GetByte(adr1)));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				case Instructions.and_as:
					SetUInt16(adr0, (ushort) (GetUInt16(adr0) & GetUInt16(adr1)));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				
				
				case Instructions.shl_vb:
					SetByte(adr0, (byte) (GetByte(adr0) << val1));
					if(InstructionPointer == prevIp) InstructionPointer += 4;
					break;
				case Instructions.shl_vs:
					SetUInt16(adr0, (ushort) (GetUInt16(adr0) << val1s));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				case Instructions.shl_ab:
					SetByte(adr0, (byte) (GetByte(adr0) << GetByte(adr1)));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				case Instructions.shl_as:
					SetUInt16(adr0, (ushort) (GetUInt16(adr0) << GetUInt16(adr1)));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				
				case Instructions.shr_vb:
					SetByte(adr0, (byte) (GetByte(adr0) >> val1));
					if(InstructionPointer == prevIp) InstructionPointer += 4;
					break;
				case Instructions.shr_vs:
					SetUInt16(adr0, (ushort) (GetUInt16(adr0) >> val1s));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				case Instructions.shr_ab:
					SetByte(adr0, (byte) (GetByte(adr0) >> GetByte(adr1)));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				case Instructions.shr_as:
					SetUInt16(adr0, (ushort) (GetUInt16(adr0) >> GetUInt16(adr1)));
					if(InstructionPointer == prevIp) InstructionPointer += 5;
					break;
				
				
				case Instructions.jl_vb:
					InstructionPointer = (GetByte(adr0) < val1)
						? GetUInt16((ushort) (InstructionPointer + 4))
						: (ushort) (InstructionPointer + 6);
					break;
				case Instructions.jl_vs:
					InstructionPointer = (GetUInt16(adr0) < val1s)
						? GetUInt16((ushort) (InstructionPointer + 5))
						: (ushort) (InstructionPointer + 7);
					break;
				case Instructions.jl_ab:
					InstructionPointer = (GetByte(adr0) < GetByte(adr1))
						? GetUInt16((ushort) (InstructionPointer + 5))
						: (ushort) (InstructionPointer + 7);
					break;
				case Instructions.jl_as:
					InstructionPointer = (GetUInt16(adr0) < GetUInt16(adr1))
						? GetUInt16((ushort) (InstructionPointer + 5))
						: (ushort) (InstructionPointer + 7);
					break;
				
				case Instructions.jg_vb:
					InstructionPointer = (GetByte(adr0) > val1)
						? GetUInt16((ushort) (InstructionPointer + 4))
						: (ushort) (InstructionPointer + 6);
					break;
				case Instructions.jg_vs:
					InstructionPointer = (GetUInt16(adr0) > val1s)
						? GetUInt16((ushort) (InstructionPointer + 5))
						: (ushort) (InstructionPointer + 7);
					break;
				case Instructions.jg_ab:
					InstructionPointer = (GetByte(adr0) > GetByte(adr1))
						? GetUInt16((ushort) (InstructionPointer + 5))
						: (ushort) (InstructionPointer + 7);
					break;
				case Instructions.jg_as:
					InstructionPointer = (GetUInt16(adr0) > GetUInt16(adr1))
						? GetUInt16((ushort) (InstructionPointer + 5))
						: (ushort) (InstructionPointer + 7);
					break;
				
				case Instructions.je_vb:
					InstructionPointer = (GetByte(adr0) == val1)
						? GetUInt16((ushort) (InstructionPointer + 4))
						: (ushort) (InstructionPointer + 6);
					break;
				case Instructions.je_vs:
					InstructionPointer = (GetUInt16(adr0) == val1s)
						? GetUInt16((ushort) (InstructionPointer + 5))
						: (ushort) (InstructionPointer + 7);
					break;
				case Instructions.je_ab:
					InstructionPointer = (GetByte(adr0) == GetByte(adr1))
						? GetUInt16((ushort) (InstructionPointer + 5))
						: (ushort) (InstructionPointer + 7);
					break;
				case Instructions.je_as:
					InstructionPointer = (GetUInt16(adr0) == GetUInt16(adr1))
						? GetUInt16((ushort) (InstructionPointer + 5))
						: (ushort) (InstructionPointer + 7);
					break;
				
				case Instructions.jne_vb:
					InstructionPointer = (GetByte(adr0) != val1)
						? GetUInt16((ushort) (InstructionPointer + 4))
						: (ushort) (InstructionPointer + 6);
					break;
				case Instructions.jne_vs:
					InstructionPointer = (GetUInt16(adr0) != val1s)
						? GetUInt16((ushort) (InstructionPointer + 5))
						: (ushort) (InstructionPointer + 7);
					break;
				case Instructions.jne_ab:
					InstructionPointer = (GetByte(adr0) != GetByte(adr1))
						? GetUInt16((ushort) (InstructionPointer + 5))
						: (ushort) (InstructionPointer + 7);
					break;
				case Instructions.jne_as:
					InstructionPointer = (GetUInt16(adr0) != GetUInt16(adr1))
						? GetUInt16((ushort) (InstructionPointer + 5))
						: (ushort) (InstructionPointer + 7);
					break;
				
				default: goto case Instructions.nop;
			}
		}
	}

	public static class MCAVirtualMachineDisassembler
	{
		public enum ArgType : byte
		{
			None,
			Byte,
			Short,
			Address
		}
		
		public static byte GetLength(this MCAVirtualMachine.Instructions instruction)
		{
			return (byte) (1 + instruction.GetArgType(0).GetLength()
			                 + instruction.GetArgType(1).GetLength()
			                 + instruction.GetArgType(2).GetLength());
		}

		public static ushort GetLength(this ArgType arg) => arg switch
		{
			ArgType.None => (ushort) 0,
			ArgType.Byte => (ushort) 1,
			ArgType.Short => (ushort) 2,
			ArgType.Address => (ushort) 2,
			_ => (ushort) 0
		};

		public static ArgType GetArgType(this MCAVirtualMachine.Instructions instruction, byte argn)
		{
			if (argn > 2 || instruction == MCAVirtualMachine.Instructions.nop) return ArgType.None;
			if (instruction == MCAVirtualMachine.Instructions.not_b ||
			    instruction == MCAVirtualMachine.Instructions.not_s)
				return argn == 0 ? ArgType.Address : ArgType.None;

			if (instruction >= MCAVirtualMachine.Instructions.mov_vb &&
			    instruction <= MCAVirtualMachine.Instructions.jne_as)
				return argn switch
				{
					0 => ArgType.Address,
					1 => ((byte) instruction & 0b10) == 0b10
						? ArgType.Address
						: ((byte) instruction & 0b1) == 0b0
							? ArgType.Byte
							: ArgType.Short,
					2 when instruction >= MCAVirtualMachine.Instructions.jl_vb => ArgType.Address,
					_ => ArgType.None
				};

			return ArgType.None;
		}
		
		public static string ReadInstruction(this MCAVirtualMachine vm, short byteIndex)
		{
			return null;
		}

		public static string ReadArg(BinaryReader reader, ArgType argType)
		{
			return argType == ArgType.Byte
				? reader.ReadByte().ToString("x2")
				: (argType == ArgType.Short || argType == ArgType.Address)
					? reader.ReadUInt16().ToString("x4")
					: "";
		}

		public static string ReadInstruction(BinaryReader reader)
		{
			var instruction = (MCAVirtualMachine.Instructions) reader.ReadByte();
			var arg0 = instruction.GetArgType(0);
			var arg1 = instruction.GetArgType(1);
			var arg2 = instruction.GetArgType(2);
			return $"{instruction} {ReadArg(reader, arg0)}\t{ReadArg(reader, arg1)}\t{ReadArg(reader, arg2)}".Trim();
		}
	}
}