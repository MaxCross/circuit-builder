﻿#define Debug

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using OrbitalShipHacker.Engine;

namespace OrbitalShipHacker.Graphics
{
	public class CircuitForm : Form
	{
		public Level Level;
		
		public Player Player => Level.Player;
		public CircuitPlane Board => Level.Board;
		
		private int selectedSlot = -1;
		public int SelectedSlot
		{
			get
			{
				if (selectedSlot >= Player.Items.Count)
					selectedSlot = Player.Items.Count - 1;
				if (selectedSlot < -1)
					selectedSlot = -1;
				return selectedSlot;
			}
			set => selectedSlot = value;
		}

		private DrawingPanel boardPannel;
		private DrawingPanel creativeItemsPannel;
		private DrawingPanel itemsPannel;
		private RichTextBox levelDescriptionControl;
		
#if Debug
		private Label debugLabel;
#endif
		
		public const int BlockSize = 8;
		public Vector2i ItemSlotSize = new Vector2i(150, 64);

		public Timer Ticker;

		public CircuitForm() : this(new Level(new Player(), new CircuitPlane(new Vector2i(32, 32)))) {}
		public bool Paused = false;

		private List<IItem> creativeItems;

		public void SetCreativeItems()
		{
			creativeItems = new List<IItem>()
			{
				new WirePrinter(),
				new AccessEditor(),
				
				new PlacableElement(new MCA()),
				new PlacableElement(new MagicClock()),
				new PlacableElement(new Indicator()),
				new PlacableElement(new LevelTarget()),
			};
		}

		public CircuitForm(Level level)
		{
			SetCreativeItems();
			// SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
			Level = level;
			
			Ticker = new Timer();
			Ticker.Interval = 200;
			Ticker.Tick += (sender, args) => Tick();
			Ticker.Start();
			
			MinimumSize = Board.Size * BlockSize + new Vector2i(2 + ItemSlotSize.X + 2 + ItemSlotSize.X + 16, 59 + 200);
			Resize += (sender, args) => PlaceElements();
			BackColor = Color.Black;
			
			var fileMenuItem = new MenuItem("Файл");
			
			var loadCreativeMenuItem = new MenuItem("Загрузить уровень для редактирования");
			loadCreativeMenuItem.Click += (sender, args) => LoadLevel(true);
			fileMenuItem.MenuItems.Add(loadCreativeMenuItem);
			
			var loadBasicMenuItem = new MenuItem("Загрузить уровень для прохождения");
			loadBasicMenuItem.Click += (sender, args) => LoadLevel(false);
			fileMenuItem.MenuItems.Add(loadBasicMenuItem);
			
			var saveMenuItem = new MenuItem("Сохранить уровень");
			saveMenuItem.Click += (sender, args) => SaveLevel();
			fileMenuItem.MenuItems.Add(saveMenuItem);
			
			Menu = new MainMenu(new []{fileMenuItem});
			
			boardPannel = new DrawingPanel();
			boardPannel.Paint += (sender, args) => DrawBoard(args.Graphics);
			boardPannel.MouseDown += (sender, args) => OnBoardMouseDown(args);
			boardPannel.MouseUp += (sender, args) => OnBoardMouseUp(args);
			// boardPannel.MouseClick += (sender, args) => OnClickToBoard(args);
			boardPannel.MouseMove += (sender, args) => OnBoardMouseMove(args);
			boardPannel.MouseLeave += (sender, args) => OnBoardMouseLeave();
			boardPannel.MouseWheel += (sender, args) => OnBoardScroll(args);
			Controls.Add(boardPannel);
			
			creativeItemsPannel = new DrawingPanel();
			creativeItemsPannel.BackColor = Color.DimGray;
			creativeItemsPannel.Paint += (sender, args) => DrawCreativeItems(args.Graphics);
			creativeItemsPannel.MouseClick += (sender, args) => OnClickToCreativeItemsPannel(args);
			Controls.Add(creativeItemsPannel);
			
			itemsPannel = new DrawingPanel();
			itemsPannel.BackColor = Color.DimGray;
			itemsPannel.Paint += (sender, args) => DrawInventoryItems(args.Graphics);
			itemsPannel.MouseClick += (sender, args) => OnClickToItemsPannel(args);
			Controls.Add(itemsPannel);
			
			levelDescriptionControl = new RichTextBox();
			levelDescriptionControl.Text = Level.Description;
			levelDescriptionControl.TabStop = false;
			levelDescriptionControl.TextChanged += (sender, args) => Level.Description = levelDescriptionControl.Text;
			levelDescriptionControl.KeyDown += (sender, args) => OnLevelDescriptionKeyDown(args);
			levelDescriptionControl.Font = new Font(new FontFamily("Courier"), 16f, GraphicsUnit.Pixel);
			levelDescriptionControl.BorderStyle = BorderStyle.None;
			levelDescriptionControl.BackColor = Color.DarkBlue;
			levelDescriptionControl.ForeColor = Color.Green;
			Controls.Add(levelDescriptionControl);

			// KeyPreview = true;
			
			Level.Passed += OnLevelPassed;
			
#if Debug
			debugLabel = new Label();
			debugLabel.ForeColor = Color.Chartreuse;
			Controls.Add(debugLabel);
#endif
			
			PlaceElements();
		}

		public void PlaceElements()
		{
			boardPannel.Location = new Point(0, 0);
			boardPannel.Size = Board.Size * BlockSize;
			
			itemsPannel.Location = new Point(Width - ItemSlotSize.X - 16, 0);
			itemsPannel.Size = new Size(ItemSlotSize.X, Height);
			
			creativeItemsPannel.Location = new Point(itemsPannel.Left - ItemSlotSize.X - 2, 0);
			creativeItemsPannel.Size = new Size(ItemSlotSize.X, Height);
			
			levelDescriptionControl.Location = new Point(boardPannel.Location.X, boardPannel.Bottom + 2);
			levelDescriptionControl.Size = new Size(Width - ItemSlotSize.X - ItemSlotSize.X - 4 - 16,
				Height - Board.Size.Y * BlockSize - 4 - 59);
			
#if Debug
			debugLabel.Location = new Point(creativeItemsPannel.Left - 80, levelDescriptionControl.Top - 80);
			debugLabel.Size = new Size(80, 80);
#endif
		}

		public void OnLevelPassed()
		{
			Paused = true;
			MessageBox.Show("Passed!");
		}

		protected override void OnPreviewKeyDown(PreviewKeyDownEventArgs e)
		{
			e.IsInputKey = true;
		}

		protected override void OnKeyDown(KeyEventArgs args)
		{
			args.SuppressKeyPress = false;
			
			if (args.KeyCode == Keys.Space)
			{
				Paused = !Paused;
				Ticker.Enabled = !Paused;
				// MessageBox.Show($"Paused: {Paused}, Enabled: {Ticker.Enabled}");
			}

			if(!Paused)
			{
				if (args.KeyCode == Keys.Right)
				{
					Ticker.Interval = Ticker.Interval switch
					{
						1 => 50,
						50 => 200,
						200 => 1000,
						_ => 1000
					};
				}
				if (args.KeyCode == Keys.Left)
				{
					Ticker.Interval = Ticker.Interval switch
					{
						50 => 1,
						200 => 50,
						1000 => 200,
						_ => 1
					};
				}
			}
			else
			{
				if(args.KeyCode == Keys.Left || args.KeyCode == Keys.Right) Tick(true);
				if(args.KeyCode == Keys.Right) Tick(true);
			}
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			ActiveControl = null;
		}

		public void OnLevelDescriptionKeyDown(KeyEventArgs args)
		{
			if (args.KeyCode == Keys.Escape) ActiveControl = null;
		}

		public void OnBoardScroll(MouseEventArgs args)
		{
			var position = (Vector2i) args.Location / BlockSize;
			if (SelectedSlot != -1)
			{
				var action = args.Delta < 0 ? 4 : 5;
				Player.UseItem(Board, Level, SelectedSlot, position, action);
				boardPannel.Invalidate();
				itemsPannel.Invalidate();
			}
		}

		public void OnBoardMouseLeave()
		{
			boardBlockUnderCursor = new Vector2i(-1, -1);
		}

		public void Tick(bool ignorePause = false)
		{
			if (Paused && !ignorePause)
			{
				Ticker.Enabled = false;
				return;
			}
			Board.Tick();
			boardPannel.Invalidate();
#if Debug
			UpdateDebugInfo();
#endif
			foreach (var asmEditorForm in CurrentEditorForms) asmEditorForm?.OnTick();
		}

		public void OnClickToCreativeItemsPannel(MouseEventArgs args)
		{
			ActiveControl = null;
			if(!Player.CreativeMode) return;

			var id = GetItemsPannelItem(creativeItems, args.Y);
			if (id >= creativeItems.Count || id < 0)
			{
				if(SelectedSlot != -1)
					Player.Items.RemoveAt(SelectedSlot);
			}
			else
			{
				var itemCopy = creativeItems[id].Copy();
				if (itemCopy is PlacableElement el && el.Element is LevelTarget target) target.Level = Level; 
				Player.AddItem(itemCopy);
			}
			
			creativeItemsPannel.Invalidate();
			itemsPannel.Invalidate();
		}

		public void OnClickToItemsPannel(MouseEventArgs args)
		{
			ActiveControl = null;
			
			var id = GetItemsPannelItem(Player.Items, args.Y);
			if (id >= Player.Items.Count || id < -1 || id == selectedSlot) id = -1;
			if (args.Button == MouseButtons.Left)
			{
				SelectedSlot = id;
				itemsPannel.Invalidate();
				boardPannel.Invalidate();
			}
			else if (args.Button == MouseButtons.Right &&  id != -1)
			{
				ShowItemInfo(Player.GetItem(id));
			}
		}

		public void ShowItemInfo(IItem item)
		{
			if(item is PlacableElement pe) ShowItemInfo(pe);
		}
		
		public void ShowItemInfo(PlacableElement item)
		{
			ShowElementInfo(item.Element);
		}

		private void DrawCreativeItems(System.Drawing.Graphics graphics) => DrawItems(graphics, creativeItems);
		
		public void DrawInventoryItems(System.Drawing.Graphics graphics) => DrawItems(graphics, Player.Items, SelectedSlot);

		public void DrawItems(System.Drawing.Graphics graphics, List<IItem> items, int highlightedSlot = -1)
		{
			var curY = 0;
			for (var i = 0; i < items.Count; i++)
			{
				var item = items[i];
				var color = highlightedSlot == i ? Color.DarkGray : Color.Gray;
				var occSize = GetItemIconOccupiedSize(item, Rotations.Left);
				graphics.FillRectangle(new SolidBrush(color), 
					new Rectangle(0, curY, ItemSlotSize.X, occSize.Y + 4));
				DrawItemIcon(graphics, item, new Vector2i((ItemSlotSize.X - occSize.X) / 2, curY + 2));
				curY += occSize.Y + 4;
				color = highlightedSlot == i ? Color.Gray : Color.DarkSlateGray;
				graphics.FillRectangle(new SolidBrush(color), 
					new Rectangle(0, curY, ItemSlotSize.X, 16));
				graphics.DrawString(item.FullName,
					new Font(new FontFamily("Courier"), 12f, GraphicsUnit.Pixel),
					Brushes.Azure,
					new Rectangle(0, curY, ItemSlotSize.X, 26));
				curY += 16 + 2;
			}
		}
		
		public int GetItemsPannelItem(List<IItem> items, int y)
		{
			var curY = 0;
			var i = 0;
			foreach (var item in items)
			{
				curY += 2 + GetItemIconOccupiedSize(item, Rotations.Right).Y + 2 + 16 + 2;
				if (y < curY) return i;
				i++;
			}

			return -1;
		}

		public Vector2i GetItemIconOccupiedSize(IItem item, Rotations rotation)
		{
			if (item is PlacableElement pe)
				return (rotation == Rotations.Up || rotation == Rotations.Down)
					? pe.Element.Size * BlockSize
					: new Vector2i(pe.Element.Size.Y, pe.Element.Size.X) * BlockSize;
			if (item is WirePrinter || item is AccessEditor) return new Vector2i(5, 2) * BlockSize;
			return Vector2i.Zero;
		}

		public void DrawItemIcon(System.Drawing.Graphics graphics, IItem item, Vector2i position)
		{
			if (item is PlacableElement pe)
			{
				DrawElement(graphics, pe.Element, position, Rotations.Left);
			}

			if (item is WirePrinter printer)
			{
				graphics.FillRectangle(new SolidBrush(Color.Chocolate), 
					new Rectangle(position.X, position.Y, 4 * BlockSize, BlockSize));
				graphics.FillRectangle(new SolidBrush(Color.Chocolate), 
					new Rectangle(position.X + 3 * BlockSize, position.Y + 1 * BlockSize, 3 * BlockSize, BlockSize));
			}
			if (item is AccessEditor accessEditor)
			{
				graphics.FillRectangle(new SolidBrush(Color.Red), 
					new Rectangle(position.X, position.Y, 4 * BlockSize, BlockSize));
				graphics.FillRectangle(new SolidBrush(Color.Red), 
					new Rectangle(position.X + 3 * BlockSize, position.Y + 1 * BlockSize, 3 * BlockSize, BlockSize));
			}
		}

		public void DrawBoard(System.Drawing.Graphics graphics)
		{
			for (int x = 0; x < Board.Size.X; x++)
			{
				for (int y = 0; y < Board.Size.Y; y++)
				{
					var color = Color.Black;
					if(Board.Blocks[x, y] == Blocks.Circuit)
						color = Color.Green;
					if(Board.SignalGrid[x, y].Type == WireType.Normal)
						color = Color.Chocolate;
					if(Board.SignalGrid[x, y].Type == WireType.CrossBrige)
						color = Color.Coral;
					if (Level.AllowedArea != Rectangle.Empty && !Level.AllowedArea.Contains(new Point(x, y)))
						color = color.Highlight(Highlights.Disallow);
					
					graphics.FillRectangle(new SolidBrush(color), 
						new Rectangle(x * BlockSize, y * BlockSize, BlockSize, BlockSize));
				}
			}

			var elementUnderCursor = SelectedSlot == -1 && boardBlockUnderCursor != new Vector2i(-1, -1)
				? Board.GetElementAt(boardBlockUnderCursor) : default;
			for(var i = 0; i < Board.Elements.Length; i++)
			{
				var element = Board.Elements[i];
				if(element.Element == null) continue;
				var highlight = (elementUnderCursor.Element == element.Element)
					? Highlights.Hover
					: !Level.DisallowedElements[i] ? Highlights.None : Highlights.Disallow;
				DrawElement(graphics, element.Element, element.Position * BlockSize, element.Rotation, highlight);
			}

			var selectedItem = Player.GetItem(SelectedSlot);
			if (boardBlockUnderCursor != new Vector2i(-1, -1) && selectedItem != null)
			{
				if(selectedItem is PlacableElement pe)
				{
					var hasIntersections = Board.HasIntersections(pe.Element, boardBlockUnderCursor, pe.targetRotation);
					DrawElement(graphics, pe.Element, boardBlockUnderCursor * BlockSize,
						pe.targetRotation, hasIntersections ? Highlights.Disallow : Highlights.Allow);
				}

				if (elementUnderCursor.Element == null && selectedItem is WirePrinter printer)
				{
					if (printer.LastPoint != new Vector2i(-1, -1))
					{
						var wiresLeft = printer.WiresLeft;
						var intersected = false;
						foreach (var p in printer.LastPoint.LineTo(boardBlockUnderCursor))
						{
							if(!Board.IsPointOnCircuit(p)) break;
							if (!intersected && Board.GetElementAt(p).Element != null) intersected = true;
							var color = !intersected && wiresLeft > 0 ? Color.Chocolate : Color.Red;
							if(Board.SignalGrid[p.X, p.Y].Type == WireType.Empty)
								wiresLeft--;
							else color = Color.Red;
							graphics.FillRectangle(new SolidBrush(color), 
								new Rectangle(p.X * BlockSize, p.Y * BlockSize, BlockSize, BlockSize));
						}
					}
				}
			}
		}

		public void OnBoardMouseDown(MouseEventArgs args)
		{
			ActiveControl = null;
			
			if (args.Button == MouseButtons.XButton1)
			{
				SelectedSlot = -1;
				boardPannel.Invalidate();
				itemsPannel.Invalidate();
				return;
			}
			var position = ((Vector2i) args.Location) / BlockSize;
			
			if(boardBlockUnderCursor != new Vector2i(-1, -1) && selectedSlot == -1)
			{
				var elementIndex = Board.GetElementIndexAt(boardBlockUnderCursor);
				if(elementIndex == -1 || (!Player.CreativeMode && Level.DisallowedElements[elementIndex])) return;
				var elementUnderCursor = Board.Elements[elementIndex];

				if (args.Button == MouseButtons.Right)
				{
					ShowElementInfo(elementUnderCursor.Element);
					return;
				}

				Board.RemoveElement(elementUnderCursor.Element);
				selectedSlot = Player.AddItem(
					new PlacableElement(elementUnderCursor.Element) {targetRotation = elementUnderCursor.Rotation});
				itemsPannel.Invalidate();
				return;
			}
			
			if (SelectedSlot != -1)
			{
				var action = args.Button switch
				{
					MouseButtons.Left => 0,
					MouseButtons.Right => 1,
					_ => 0
				};
				Player.UseItem(Board, Level, SelectedSlot, position, action);
				boardPannel.Invalidate();
				itemsPannel.Invalidate();
				return;
			}
		}
		
		public void OnBoardMouseUp(MouseEventArgs args)
		{
			var position = ((Vector2i) args.Location) / BlockSize;
			if (SelectedSlot != -1)
			{
				var action = args.Button switch
				{
					MouseButtons.Left => 2,
					MouseButtons.Right => 3,
					_ => 0
				};
				Player.UseItem(Board, Level, SelectedSlot, position, action);
				boardPannel.Invalidate();
				itemsPannel.Invalidate();
				return;
			}
		}

		private Vector2i boardBlockUnderCursor = new Vector2i(-1, -1);
		
		public void OnBoardMouseMove(MouseEventArgs args)
		{
			boardBlockUnderCursor = (Vector2i) args.Location / BlockSize;

			if (SelectedSlot != -1 && Board.IsPointOnCircuit(boardBlockUnderCursor))
			{
				var action = args.Button switch
				{
					MouseButtons.Left => 6,
					MouseButtons.Right => 7,
					_ => -1
				};
				if (action != -1)
					Player.UseItem(Board, Level, SelectedSlot, boardBlockUnderCursor, action);
			}
			
#if Debug
			UpdateDebugInfo();
#endif
			boardPannel.Invalidate();
		}

#if Debug
		public void UpdateDebugInfo()
		{
			if(boardBlockUnderCursor == new Vector2i(-1, -1))
				return;
			debugLabel.Text =
@$"{boardBlockUnderCursor};
NetworkID: {Board.GetNetworkId(boardBlockUnderCursor)};
{Board.GetNetwork(boardBlockUnderCursor) == null};
Signal: {Board.GetInputSignal(boardBlockUnderCursor)};";
			
			var element = Board.GetElementAt(boardBlockUnderCursor);
			if(element.Element == null)
				return;
			debugLabel.Text += $"\nRotation: {element.Rotation};";
			if (element.Element is MCA mca)
				debugLabel.Text += $"\nip: {mca.VirtualMachine.InstructionPointer};";
		}
#endif

		public enum Highlights
		{
			None,
			Allow,
			Disallow,
			Hover
		}

		public void DrawElement(System.Drawing.Graphics graphics, ICircuitElement element, Vector2i position, Rotations rotations, Highlights highlight = Highlights.None)
		{
			if (element is MCA mca) DrawElement(graphics, mca, position, rotations, highlight);
			if (element is MagicClock magicClock) DrawElement(graphics, magicClock, position, rotations, highlight);
			if (element is Indicator indicator) DrawElement(graphics, indicator, position, rotations, highlight);
			if (element is LevelTarget checker) DrawElement(graphics, checker, position, rotations, highlight);
		}

		public void DrawElement(System.Drawing.Graphics graphics, MCA element, Vector2i position, Rotations rotation, Highlights highlight = Highlights.None)
		{
			foreach (var connection in element.ConnectionLocalPoints
				.Select(p => position + element.RotateLocalPoint(p, rotation) * BlockSize))
				graphics.FillRectangle(new SolidBrush(Color.Yellow.Highlight(highlight)),
					new Rectangle(connection.X, connection.Y, BlockSize, BlockSize));
			
			var body = new Vector2i(5, 9);
			if(rotation == Rotations.Left || rotation == Rotations.Right)
				body = new Vector2i(9, 5);
			graphics.FillRectangle(new SolidBrush(Color.Black.Highlight(highlight)),
				new Rectangle(position.X + BlockSize, position.Y + BlockSize,
					body.X * BlockSize, body.Y * BlockSize));
			
			var upIndicator = position + element.RotateLocalPoint(new Vector2i(3, 1), rotation) * BlockSize;
			graphics.FillRectangle(new SolidBrush(Color.DarkSlateGray.Highlight(highlight)),
				new Rectangle(upIndicator.X, upIndicator.Y, BlockSize, BlockSize));
		}

		public void DrawElement(System.Drawing.Graphics graphics, MagicClock element, Vector2i position, Rotations rotation, Highlights highlight = Highlights.None)
		{
			foreach (var connection in element.ConnectionLocalPoints
				.Select(p => position + element.RotateLocalPoint(p, rotation) * BlockSize))
				graphics.FillRectangle(new SolidBrush(Color.Yellow.Highlight(highlight)),
					new Rectangle(connection.X, connection.Y, BlockSize, BlockSize));
			
			var bodyPosition = position;
			if(rotation == Rotations.Right) bodyPosition += Vector2i.Right * BlockSize;
			if(rotation == Rotations.Down) bodyPosition += Vector2i.Down * BlockSize;
			graphics.FillRectangle(new SolidBrush(Color.Black.Highlight(highlight)),
				new Rectangle(bodyPosition.X, bodyPosition.Y, 3 * BlockSize, 3 * BlockSize));
			
			var led = position + element.RotateLocalPoint(new Vector2i(1, 1), rotation) * BlockSize;
			graphics.FillRectangle(new SolidBrush((element.PrevState ? Color.DarkOrange : Color.OrangeRed).Highlight(highlight)),
				new Rectangle(led.X, led.Y, BlockSize, BlockSize));
		}
		
		public void DrawElement(System.Drawing.Graphics graphics, LevelTarget element, Vector2i position, Rotations rotation, Highlights highlight = Highlights.None)
		{
			foreach (var connection in element.ConnectionLocalPoints
				.Select(p => position + element.RotateLocalPoint(p, rotation) * BlockSize))
				graphics.FillRectangle(new SolidBrush(Color.Yellow.Highlight(highlight)),
					new Rectangle(connection.X, connection.Y, BlockSize, BlockSize));
			
			var bodyPosition = position;
			if(rotation == Rotations.Right) bodyPosition += Vector2i.Right * BlockSize;
			if(rotation == Rotations.Down) bodyPosition += Vector2i.Down * BlockSize;
			graphics.FillRectangle(new SolidBrush(Color.Wheat.Highlight(highlight)),
				new Rectangle(bodyPosition.X, bodyPosition.Y, 3 * BlockSize, 3 * BlockSize));
			
			var led = position + element.RotateLocalPoint(new Vector2i(1, 1), rotation) * BlockSize;
			graphics.FillRectangle(new SolidBrush((Color.Green).Highlight(highlight)),
				new Rectangle(led.X, led.Y, BlockSize, BlockSize));
		}
		
		public void DrawElement(System.Drawing.Graphics graphics, Indicator element, Vector2i position, Rotations rotation, Highlights highlight = Highlights.None)
		{
			foreach (var connection in element.ConnectionLocalPoints
				.Select(p => position + element.RotateLocalPoint(p, rotation) * BlockSize))
				graphics.FillRectangle(new SolidBrush(Color.Yellow.Highlight(highlight)),
					new Rectangle(connection.X, connection.Y, BlockSize, BlockSize));
			
			var bodyPosition = position;
			if(rotation == Rotations.Right) bodyPosition += Vector2i.Right * BlockSize;
			if(rotation == Rotations.Down) bodyPosition += Vector2i.Down * BlockSize;
			var bodyScale = (element.Size - Vector2i.Down) * BlockSize;
			if(rotation == Rotations.Left || rotation == Rotations.Right)
				bodyScale = new Vector2i(bodyScale.Y, bodyScale.X);
			graphics.FillRectangle(new SolidBrush(Color.Black.Highlight(highlight)),
				new Rectangle(bodyPosition, bodyScale));
			
			// var led = position + element.RotateLocalPoint(new Vector2i(1, 1), rotation) * BlockSize;
			graphics.DrawString(element.state.ToString("x2"),
				new Font(new FontFamily("Courier"), 24f, GraphicsUnit.Pixel),
				Brushes.Azure,
				new Rectangle(bodyPosition, bodyScale));
		}

		public void ShowElementInfo(ICircuitElement element)
		{
			if (element is MCA mca)
			{
				ShowElementInfo(mca);
				return;
			}
		}

		public List<AsmEditorForm> CurrentEditorForms = new List<AsmEditorForm>();
		public void ShowElementInfo(MCA element)
		{
			var form = new AsmEditorForm(element.VirtualMachine);
			CurrentEditorForms.Add(form);
			form.Closed += (sender, args) => CurrentEditorForms.Remove(sender as AsmEditorForm);
			form.Show();
		}
		
		public void LoadLevel(bool creative)
		{
			var ofd = new OpenFileDialog();
			ofd.CheckFileExists = true;
			ofd.Filter = "Level file|*.level";
			
			if(ofd.ShowDialog() != DialogResult.OK) return;
			
			var fs = new FileStream(ofd.FileName, FileMode.Open);
			Level = Level.Load(new BinaryReader(fs));
			fs.Close();

			levelDescriptionControl.Text = Level.Description;
			Level.Passed += OnLevelPassed;
			
			Player.CreativeMode = creative;
			levelDescriptionControl.ReadOnly = !creative;
			
			PlaceElements();
			boardPannel.Invalidate();
			itemsPannel.Invalidate();
		}

		public void SaveLevel()
		{
			var sfd = new SaveFileDialog();
			sfd.Filter = "Level file|*.level";
			sfd.FilterIndex = 0;
			
			if(sfd.ShowDialog() != DialogResult.OK) return;

			var fs = new FileStream(sfd.FileName, FileMode.Create);
			Level.Save(new BinaryWriter(fs));
			fs.Close();
		}
	}

	public class DrawingPanel : Panel
	{
		public DrawingPanel()
		{
			DoubleBuffered = true;
		}
	}

	public static class Extensions
	{
		public static Color Highlight(this Color color, CircuitForm.Highlights highlight) => highlight switch
		{
			CircuitForm.Highlights.None => color,
			CircuitForm.Highlights.Allow => Color.FromArgb(128, color.R, Math.Min(color.G + 96, 0xff), color.B),
			CircuitForm.Highlights.Disallow => Color.FromArgb(128, Math.Min(color.R + 96, 0xff), color.G, color.B),
			CircuitForm.Highlights.Hover => Color.FromArgb(255, Math.Min(color.R + 64, 0xff), Math.Min(color.G + 64, 0xff), Math.Min(color.B + 64, 0xff)),
			_ => color
		};
	}
}