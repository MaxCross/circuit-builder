﻿using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using OrbitalShipHacker.Engine;
using OrbitalShipHacker.VM;

namespace OrbitalShipHacker.Graphics
{
	public class AsmEditorForm : Form
	{
		private MCAVirtualMachine VirtualMachine;
		public ushort Offset = 16;

		public Font MainFont = new Font(new FontFamily("Courier"), 16f, GraphicsUnit.Pixel);
		private int lineHeight => MainFont.Height;

		private DrawingPanel offsetPanel;
		private DrawingPanel hexPanel;
		private DrawingPanel asmPanel;

		public enum FocusSections : byte
		{
			None,
			Instruction,
			Arg0,
			Arg1,
			Arg2
		}

		public FocusSections CurFocusSection;
		public int FocusedDataIndex = -1;

		public string FocusText;

		public static readonly int[] AsmPannelOffsets = {0, 70, 120, 170};

		public AsmEditorForm(MCAVirtualMachine vm)
		{
			VirtualMachine = vm;
			Resize += (sender, args) => PlaceElements();
			
			offsetPanel = new DrawingPanel();
			offsetPanel.BackColor = Color.Black;
			offsetPanel.Paint += (sender, args) => DrawOffsetPanel(args.Graphics);
			Controls.Add(offsetPanel);
			
			hexPanel = new DrawingPanel();
			hexPanel.BackColor = Color.Black;
			hexPanel.Paint += (sender, args) => DrawHexPanel(args.Graphics);
			Controls.Add(hexPanel);
			
			asmPanel = new DrawingPanel();
			asmPanel.BackColor = Color.Black;
			asmPanel.MouseWheel += (sender, args) => OnAsmPanelMouseWheel(args);
			asmPanel.MouseClick += (sender, args) => OnClickToAsmPanel(args);
			asmPanel.Paint += (sender, args) => DrawAsmPanel(args.Graphics);
			Controls.Add(asmPanel);
			
			PlaceElements();
		}

		public void PlaceElements()
		{
			offsetPanel.Location = new Point(0, 0);
			offsetPanel.Size = new Size(50, Bottom);
			
			hexPanel.Location = new Point(offsetPanel.Right + 2, 0);
			hexPanel.Size = new Size(180, Bottom);
			
			asmPanel.Location = new Point(hexPanel.Right + 2, 0);
			asmPanel.Size = new Size(220, Bottom);

			MinimumSize = new Size(asmPanel.Right + 16, 500);
			
			offsetPanel.Invalidate();
			hexPanel.Invalidate();
			asmPanel.Invalidate();
		}

		private void OnAsmPanelMouseWheel(MouseEventArgs args)
		{
			if (args.Delta < 0)
				Offset++;
			else if(Offset != 0) Offset--;
			if (Offset >= VirtualMachine.DataLength) Offset = VirtualMachine.DataLength;
			if (Offset < 0) Offset = 0;
			
			offsetPanel.Invalidate();
			hexPanel.Invalidate();
			asmPanel.Invalidate();
		}

		public void OnClickToAsmPanel(MouseEventArgs args)
		{
			FocusApplySection();
			var line = args.Y / lineHeight;
			var offset = FocusSections.Instruction;
			for (int i = 1; i <= 3; i++)
				if (args.X >= AsmPannelOffsets[i])
					offset = (FocusSections) i + 1;
			SetFocus(line, offset);
			asmPanel.Invalidate();
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
			if(e.KeyCode == Keys.Enter) { FocusApplySection(); ClearFocus(); }
			if(e.KeyCode == Keys.Escape)
			{
				if(CurFocusSection != FocusSections.None) ClearFocus();
				else Close();
			}
			if(e.KeyCode == Keys.Left) FocusMoveToPrevSection();
			if(e.KeyCode == Keys.Right) FocusMoveToNextSection();
			if(e.KeyCode == Keys.Up) FocusMoveToPrevLine(CurFocusSection);
			if(e.KeyCode == Keys.Down) FocusMoveToNextLine(CurFocusSection);
			asmPanel.Invalidate();
		}

		protected override void OnKeyPress(KeyPressEventArgs e)
		{
			if (CurFocusSection != FocusSections.None)
			{
				if (e.KeyChar == '\b') FocusText = "";
				else if (e.KeyChar == ' ') FocusMoveToNextSection();

				else FocusText += e.KeyChar;
				asmPanel.Invalidate();
			}
		}

		public int GetLineAtDataIndex(ushort dataIndex)
		{
			if (dataIndex < Offset) return -1;
			var curDataIndex = Offset;
			for (var i = 0; i < 128; i++)
			{
				if (curDataIndex >= VirtualMachine.DataLength) return -1;
				if (curDataIndex == dataIndex) return i;
				curDataIndex += ((MCAVirtualMachine.Instructions) VirtualMachine.GetByte(curDataIndex)).GetLength();
			}

			return -1;
		}
		
		public ushort GetDataIndexAtLine(int line)
		{
			var curDataIndex = Offset;
			for (var i = 0; i < 128; i++)
			{
				if (curDataIndex >= VirtualMachine.DataLength) return 0;
				if (i == line) return curDataIndex;
				curDataIndex += ((MCAVirtualMachine.Instructions) VirtualMachine.GetByte(curDataIndex)).GetLength();
			}

			return 0;
		}

		public void SetFocus(int line, FocusSections section = FocusSections.Instruction)
		{
			if(line < 0) ClearFocus();
			else SetFocus(GetDataIndexAtLine(line), section);
		}

		public void SetFocus(ushort dataIndex, FocusSections section = FocusSections.Instruction)
		{
			FocusedDataIndex = dataIndex;
			var curInstruction = (MCAVirtualMachine.Instructions) VirtualMachine.GetByte((ushort) FocusedDataIndex);
			
			CurFocusSection = section;
			if (CurFocusSection == FocusSections.Arg2
			    && curInstruction.GetArgType(2) == MCAVirtualMachineDisassembler.ArgType.None)
				CurFocusSection = FocusSections.Arg1;
			if (CurFocusSection == FocusSections.Arg1
			    && curInstruction.GetArgType(1) == MCAVirtualMachineDisassembler.ArgType.None)
				CurFocusSection = FocusSections.Arg0;
			if (CurFocusSection == FocusSections.Arg0
			    && curInstruction.GetArgType(0) == MCAVirtualMachineDisassembler.ArgType.None)
				CurFocusSection = FocusSections.Instruction;
			FocusText = "";
		}

		public void ClearFocus()
		{
			FocusedDataIndex = -1;
			CurFocusSection = FocusSections.None;
			FocusText = "";
		}

		public void FocusMoveToPrevLine(FocusSections section = FocusSections.Instruction)
			=> SetFocus(GetLineAtDataIndex((ushort) FocusedDataIndex) - 1,
				section == FocusSections.None ? FocusSections.Instruction : section);

		public void FocusMoveToNextLine(FocusSections section = FocusSections.Instruction)
			=> SetFocus(GetLineAtDataIndex((ushort) FocusedDataIndex) + 1,
				section == FocusSections.None ? FocusSections.Instruction : section);

		public void FocusMoveToPrevSection()
		{
			if (CurFocusSection == FocusSections.None || FocusedDataIndex == -1) return;
			FocusApplySection();
			FocusText = "";
			
			CurFocusSection = CurFocusSection switch
			{
				FocusSections.Arg2 => FocusSections.Arg1,
				FocusSections.Arg1 => FocusSections.Arg0,
				FocusSections.Arg0 => FocusSections.Instruction,
				FocusSections.Instruction => FocusSections.Arg2
			};
			if(CurFocusSection == FocusSections.Arg2)
				FocusMoveToPrevLine(CurFocusSection);
		}

		public void FocusMoveToNextSection()
		{
			if(CurFocusSection == FocusSections.None || FocusedDataIndex == -1) return;
			FocusApplySection();
			var curInstruction = (MCAVirtualMachine.Instructions) VirtualMachine.GetByte((ushort) FocusedDataIndex);
			FocusText = "";
			if (CurFocusSection == FocusSections.Instruction
			    && curInstruction.GetArgType(0) == MCAVirtualMachineDisassembler.ArgType.None ||
			    CurFocusSection == FocusSections.Arg0
			    && curInstruction.GetArgType(1) == MCAVirtualMachineDisassembler.ArgType.None ||
			    CurFocusSection == FocusSections.Arg1
			    && curInstruction.GetArgType(2) == MCAVirtualMachineDisassembler.ArgType.None ||
			    CurFocusSection == FocusSections.Arg2)
			{
				FocusMoveToNextLine();
				return;
			}

			CurFocusSection = CurFocusSection switch
			{
				FocusSections.Instruction => FocusSections.Arg0,
				FocusSections.Arg0 => FocusSections.Arg1,
				FocusSections.Arg1 => FocusSections.Arg2
			};
		}

		public void FocusApplySection()
		{
			if(CurFocusSection == FocusSections.None || FocusedDataIndex == -1) return;
			if (CurFocusSection == FocusSections.Instruction)
			{
				if (!Enum.TryParse(FocusText, true, out MCAVirtualMachine.Instructions instruction))
				{
					// VirtualMachine.SetByte((ushort) FocusedDataIndex, (byte) MCAVirtualMachine.Instructions.nop);
					return;
				}
				VirtualMachine.SetByte((ushort) FocusedDataIndex, (byte) instruction);
				return;
			}

			var curInstruction = (MCAVirtualMachine.Instructions) VirtualMachine.GetByte((ushort) FocusedDataIndex);

			ushort argOffset = 1;

			if (CurFocusSection == FocusSections.Arg1 || CurFocusSection == FocusSections.Arg2)
				argOffset += curInstruction.GetArgType(0).GetLength();
			if (CurFocusSection == FocusSections.Arg2)
				argOffset += curInstruction.GetArgType(1).GetLength();

			byte argIndex = (byte) (CurFocusSection switch
			{
				FocusSections.Arg0 => 0,
				FocusSections.Arg1 => 1,
				FocusSections.Arg2 => 2,
			});
			
			var argType = curInstruction.GetArgType(argIndex);
			if(argType == MCAVirtualMachineDisassembler.ArgType.None) return;
			if(argType == MCAVirtualMachineDisassembler.ArgType.Byte)
			{
				if (!byte.TryParse(FocusText, NumberStyles.HexNumber, CultureInfo.InvariantCulture, out var arg))
					return;//arg = 0;
				VirtualMachine.SetByte((ushort) (FocusedDataIndex + argOffset), arg);
				return;
			}
			if(argType == MCAVirtualMachineDisassembler.ArgType.Short || argType == MCAVirtualMachineDisassembler.ArgType.Address)
			{
				if (!ushort.TryParse(FocusText, NumberStyles.HexNumber, CultureInfo.InvariantCulture, out var arg))
					return;//arg = 0;
				VirtualMachine.SetUInt16((ushort) (FocusedDataIndex + argOffset), arg);
				return;
			}
		}

		public void DrawOffsetPanel(System.Drawing.Graphics graphics)
		{
			var curOffset = Offset;
			for (int i = 0; i < 64; i++)
			{
				if(curOffset >= VirtualMachine.DataLength) break;
				
				if(curOffset == VirtualMachine.InstructionPointer)
					graphics.FillRectangle(new SolidBrush(Color.DarkBlue), 
						new Rectangle(0, i * lineHeight, (int) graphics.ClipBounds.Width, lineHeight));
				
				DrawText(graphics, $"{curOffset:x4}",
					new Vector2i(0, i * lineHeight), Color.DarkGreen);
				curOffset += ((MCAVirtualMachine.Instructions) VirtualMachine.GetByte(curOffset)).GetLength();
			}
		}

		public void DrawHexPanel(System.Drawing.Graphics graphics)
		{
			var curOffset = Offset;
			for (int i = 0; i < 64; i++)
			{
				if(curOffset >= VirtualMachine.DataLength) break;

				var byteTextSize = 20;//(int) graphics.MeasureString("ff", MainFont).Width;
				var textOffset = 0;
				
				if(curOffset == VirtualMachine.InstructionPointer)
					graphics.FillRectangle(new SolidBrush(Color.DarkSlateGray), 
						new Rectangle(0, i * lineHeight, (int) graphics.ClipBounds.Width, lineHeight));
				
				var curInstruction = (MCAVirtualMachine.Instructions) VirtualMachine.GetByte(curOffset);
				DrawText(graphics, $"{(byte) curInstruction:x2}",
					new Vector2i(textOffset, i * lineHeight), Color.YellowGreen);
				textOffset += byteTextSize + 10;

				var argOffset = (ushort) 1;
				for (byte j = 0; j < 3; j++)
				{
					var curArgType = curInstruction.GetArgType(j);
					if(curArgType == MCAVirtualMachineDisassembler.ArgType.None) break;
					var color = curArgType switch
					{
						MCAVirtualMachineDisassembler.ArgType.Byte => Color.DarkGreen,
						MCAVirtualMachineDisassembler.ArgType.Short => Color.DarkGreen,
						MCAVirtualMachineDisassembler.ArgType.Address => Color.BlueViolet,
						_ => Color.Red
					};

					for (int k = 0; k < curArgType.GetLength(); k++)
					{
						DrawText(graphics, $"{VirtualMachine.GetByte((ushort) (curOffset + argOffset)):x2}",
							new Vector2i(textOffset, i * lineHeight), color);
						argOffset++;
						textOffset += byteTextSize;
					}

					textOffset += 10;
				}
				
				curOffset += ((MCAVirtualMachine.Instructions) VirtualMachine.GetByte(curOffset)).GetLength();
			}
		}

		private FocusSections GetArgFocusSection(int arg) => arg switch
		{
			0 => FocusSections.Arg0,
			1 => FocusSections.Arg1,
			2 => FocusSections.Arg2,
			_ => FocusSections.None
		};
		
		public void DrawAsmPanel(System.Drawing.Graphics graphics)
		{
			var curOffset = Offset;
			for (int i = 0; i < 64; i++)
			{
				if(curOffset >= VirtualMachine.DataLength) break;
				
				if(curOffset == FocusedDataIndex)
					graphics.FillRectangle(new SolidBrush(Color.Gray), 
						new Rectangle(0, i * lineHeight, (int) graphics.ClipBounds.Width, lineHeight));
				
				var curInstruction = (MCAVirtualMachine.Instructions) VirtualMachine.GetByte(curOffset);

				var instructionFocused = (curOffset == FocusedDataIndex && CurFocusSection == FocusSections.Instruction);

				var instructionText = (instructionFocused && FocusText != "") ? FocusText : $"{curInstruction}";
				var instructionColor = (!instructionFocused)
					? Color.YellowGreen
					: (FocusText == "" ? Color.DarkGray : Color.Blue);
				
				DrawText(graphics, instructionText,
					new Vector2i(AsmPannelOffsets[0], i * lineHeight), instructionColor);

				var argOffset = (ushort) 1;
				for (byte j = 0; j < 3; j++)
				{
					var curArgType = curInstruction.GetArgType(j);
					if(curArgType == MCAVirtualMachineDisassembler.ArgType.None) break;
					var argFocused = (curOffset == FocusedDataIndex && CurFocusSection == GetArgFocusSection(j));
					var argColor = (!argFocused)
						? (curArgType == MCAVirtualMachineDisassembler.ArgType.Address) ? Color.BlueViolet : Color.Green
						: (FocusText != "" ? Color.Blue : Color.DarkGray);
					if(curArgType == MCAVirtualMachineDisassembler.ArgType.Byte)
					{
						var argText = !argFocused || FocusText == "" 
							? $"{VirtualMachine.GetByte((ushort) (curOffset + argOffset)):x2}"
							: FocusText;
						DrawText(graphics, argText,
							new Vector2i(AsmPannelOffsets[j + 1], i * lineHeight), argColor);
						argOffset++;
					}
					if(curArgType == MCAVirtualMachineDisassembler.ArgType.Short)
					{
						var argText = !argFocused || FocusText == ""
							? $"{VirtualMachine.GetUInt16((ushort) (curOffset + argOffset)):x4}"
							: FocusText;
						DrawText(graphics, argText,
							new Vector2i(AsmPannelOffsets[j + 1], i * lineHeight), argColor);
						argOffset += 2;
					}
					if(curArgType == MCAVirtualMachineDisassembler.ArgType.Address)
					{
						var argText = !argFocused || FocusText == ""
							? $"{VirtualMachine.GetUInt16((ushort) (curOffset + argOffset)):x4}"
							: FocusText;
						DrawText(graphics, argText,
							new Vector2i(AsmPannelOffsets[j + 1], i * lineHeight), argColor);
						argOffset += 2;
					}
				}
				
				curOffset += ((MCAVirtualMachine.Instructions) VirtualMachine.GetByte(curOffset)).GetLength();
			}
		}

		public void OnTick()
		{
			offsetPanel.Invalidate();
			hexPanel.Invalidate();
			asmPanel.Invalidate();
		}

		public void DrawText(System.Drawing.Graphics graphics, string text, Vector2i point, Color color)
		{
			graphics.DrawString(text, MainFont,
				new SolidBrush(color), 
				new RectangleF(point.X, point.Y, graphics.ClipBounds.Width, lineHeight));
		}
	}
}