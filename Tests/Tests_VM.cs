﻿using NUnit.Framework;
using OrbitalShipHacker.Engine;
using OrbitalShipHacker.VM;

namespace OrbitalShipHacker.Tests
{
	public class Tests_VM
	{
		[Test]
		public void BasicCommandsTest()
		{
			var vm = new MCAVirtualMachine(new byte[]
			{
				0x10,0x00,//Instruction pointer = 0x0010
				0b0100_0100,//IO bitmask
				0x00, 0x00, 0x00, 0x00, 0x00,//reserved
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,//io ports
				(byte) MCAVirtualMachine.Instructions.mov_vb,     0x02,0x00,    0xff,
				(byte) MCAVirtualMachine.Instructions.mov_vs,     0x08,0x00,    0x12,0x13,
				(byte) MCAVirtualMachine.Instructions.je_vb,      0x08,0x00,    0x12,          0x21,0x00,
				(byte) MCAVirtualMachine.Instructions.nop,
				(byte) MCAVirtualMachine.Instructions.nop,
				(byte) MCAVirtualMachine.Instructions.jne_vb,      0x09,0x00,    0x12,          0x10,0x00,
			});
			
			vm.ExecuteCurrentInstruction();
			Assert.AreEqual(0xff, vm.GetByte(0x0002));
			
			vm.ExecuteCurrentInstruction();
			Assert.AreEqual(0x1312, vm.GetUInt16(0x0008));
			
			vm.ExecuteCurrentInstruction();
			Assert.AreEqual(0x21, vm.InstructionPointer);
			
			vm.ExecuteCurrentInstruction();
			Assert.AreEqual(0x10, vm.InstructionPointer);
		}
		
		[Test]
		public void NotMovCommandsTest()
		{
			var vm = new MCAVirtualMachine(new byte[]
			{
				0x01,0x00,//Instruction pointer = 0x0001 (set to 01 to test for no-operation instructions)
				0x00,//IO bitmask
				0x00, 0x00, 0x00, 0x00, 0x00,//reserved
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,//io ports
				(byte) MCAVirtualMachine.Instructions.not_b,      0x03,0x00,
				(byte) MCAVirtualMachine.Instructions.mov_vs,     0x03,0x00,    0b1011_1101,0b0110_1100,
				(byte) MCAVirtualMachine.Instructions.not_s,      0x03,0x00,
				
				(byte) MCAVirtualMachine.Instructions.mov_as,     0x05,0x00,    0x03,0x00,
				
				(byte) MCAVirtualMachine.Instructions.mov_vb,     0x03,0x00,    0x00,
				(byte) MCAVirtualMachine.Instructions.mov_vb,     0x04,0x00,    0x00,
				(byte) MCAVirtualMachine.Instructions.mov_vs,     0x05,0x00,    0x00,0x00,
			});

			var expectedIp = vm.InstructionPointer;
			while (expectedIp < 0x10)
			{
				vm.ExecuteCurrentInstruction();
				expectedIp++;
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
			}
			
			vm.ExecuteCurrentInstruction();
			expectedIp += MCAVirtualMachine.Instructions.not_b.GetLength();
			Assert.AreEqual(expectedIp, vm.InstructionPointer);
			Assert.AreEqual(0xff, vm.GetByte(0x03));
			
			vm.ExecuteCurrentInstruction();
			expectedIp += MCAVirtualMachine.Instructions.mov_vs.GetLength();
			Assert.AreEqual(expectedIp, vm.InstructionPointer);
			Assert.AreEqual(0b0110_1100__1011_1101, vm.GetUInt16(0x03));
			
			vm.ExecuteCurrentInstruction();
			expectedIp += MCAVirtualMachine.Instructions.not_s.GetLength();
			Assert.AreEqual(expectedIp, vm.InstructionPointer);
			Assert.AreEqual(0b1001_0011__0100_0010, vm.GetUInt16(0x03));
			
			vm.ExecuteCurrentInstruction();
			expectedIp += MCAVirtualMachine.Instructions.mov_as.GetLength();
			Assert.AreEqual(expectedIp, vm.InstructionPointer);
			Assert.AreEqual(vm.GetUInt16(0x03), vm.GetUInt16(0x05));
			
			vm.ExecuteCurrentInstruction();
			expectedIp += MCAVirtualMachine.Instructions.mov_vb.GetLength();
			Assert.AreEqual(expectedIp, vm.InstructionPointer);
			Assert.AreEqual(0, vm.GetByte(0x03));
			
			vm.ExecuteCurrentInstruction();
			expectedIp += MCAVirtualMachine.Instructions.mov_vb.GetLength();
			Assert.AreEqual(expectedIp, vm.InstructionPointer);
			Assert.AreEqual(0, vm.GetByte(0x04));
			
			vm.ExecuteCurrentInstruction();
			expectedIp += MCAVirtualMachine.Instructions.mov_vs.GetLength();
			Assert.AreEqual(expectedIp, vm.InstructionPointer);
			Assert.AreEqual(0, vm.GetUInt16(0x05));
		}
		
		[Test]
		public void AddCommandsTest()
		{
			var vm = new MCAVirtualMachine(new byte[]
			{
				0x10,0x00,//Instruction pointer = 0x0010
				0b0100_0100,//IO bitmask
				0x00, 0x00, 0x00, 0x00, 0x00,//reserved
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,//io ports
				(byte) MCAVirtualMachine.Instructions.add_vb,     0x03,0x00,    0x10,
				(byte) MCAVirtualMachine.Instructions.add_vs,     0x04,0x00,    0x11,0x00,
				
				(byte) MCAVirtualMachine.Instructions.add_ab,     0x06,0x00,    0x03,0x00,
				(byte) MCAVirtualMachine.Instructions.add_as,     0x07,0x00,    0x04,0x00,
				
				(byte) MCAVirtualMachine.Instructions.mov_vs,     0x00,0x00,    0x10,0x00,//ip = 0x10 (jump to 0x10)
			});

			byte expected03 = 0;
			ushort expected04 = 0;
			byte expected06 = 0;
			ushort expected07 = 0;

			var expectedIp = 0x10;

			for (int i = 0; i < 0xffff; i++)
			{
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_vb.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected03 += 0x10;
				Assert.AreEqual(expected03, vm.GetByte(0x0003));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_vs.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected04 += 0x0011;
				Assert.AreEqual(expected04, vm.GetUInt16(0x0004));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_ab.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected06 += expected03;
				Assert.AreEqual(expected06, vm.GetByte(0x0006));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_as.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected07 += expected04;
				Assert.AreEqual(expected07, vm.GetUInt16(0x0007));
				
				vm.ExecuteCurrentInstruction();
				expectedIp = 0x10;
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
			}
		}
		
		[Test]
		public void SubstractCommandsTest()
		{
			var vm = new MCAVirtualMachine(new byte[]
			{
				0x10,0x00,//Instruction pointer = 0x0010
				0b0100_0100,//IO bitmask
				0x00, 0x00, 0x00, 0x00, 0x00,//reserved
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,//io ports
				(byte) MCAVirtualMachine.Instructions.sub_vb,     0x03,0x00,    0x10,
				(byte) MCAVirtualMachine.Instructions.sub_vs,     0x04,0x00,    0x11,0x00,
				
				(byte) MCAVirtualMachine.Instructions.sub_ab,     0x06,0x00,    0x03,0x00,
				(byte) MCAVirtualMachine.Instructions.sub_as,     0x07,0x00,    0x04,0x00,
				
				(byte) MCAVirtualMachine.Instructions.mov_vs,     0x00,0x00,    0x10,0x00,//ip = 0x10 (jump to 0x10)
			});

			byte expected03 = 0;
			ushort expected04 = 0;
			byte expected06 = 0;
			ushort expected07 = 0;

			var expectedIp = 0x10;

			for (int i = 0; i < 0xffff; i++)
			{
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.sub_vb.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected03 -= 0x10;
				Assert.AreEqual(expected03, vm.GetByte(0x0003));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.sub_vs.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected04 -= 0x0011;
				Assert.AreEqual(expected04, vm.GetUInt16(0x0004));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.sub_ab.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected06 -= expected03;
				Assert.AreEqual(expected06, vm.GetByte(0x0006));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.sub_as.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected07 -= expected04;
				Assert.AreEqual(expected07, vm.GetUInt16(0x0007));
				
				vm.ExecuteCurrentInstruction();
				expectedIp = 0x10;
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
			}
		}
		
		[Test]
		public void MultiplyCommandsTest()
		{
			var vm = new MCAVirtualMachine(new byte[]
			{
				0x10,0x00,//Instruction pointer = 0x0010
				0b0100_0100,//IO bitmask
				0x01, 0x01, 0x00, 0x01, 0x01,//reserved
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,//io ports
				(byte) MCAVirtualMachine.Instructions.mul_vb,     0x03,0x00,    0x10,
				(byte) MCAVirtualMachine.Instructions.mul_vs,     0x04,0x00,    0x11,0x00,
				
				(byte) MCAVirtualMachine.Instructions.mul_ab,     0x06,0x00,    0x03,0x00,
				(byte) MCAVirtualMachine.Instructions.mul_as,     0x07,0x00,    0x04,0x00,
				
				(byte) MCAVirtualMachine.Instructions.mov_vs,     0x00,0x00,    0x10,0x00,//ip = 0x10 (jump to 0x10)
			});

			byte expected03 = 1;
			ushort expected04 = 1;
			byte expected06 = 1;
			ushort expected07 = 1;

			var expectedIp = 0x10;

			for (int i = 0; i < 0xffff; i++)
			{
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.mul_vb.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected03 *= 0x10;
				Assert.AreEqual(expected03, vm.GetByte(0x0003));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.mul_vs.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected04 *= 0x0011;
				Assert.AreEqual(expected04, vm.GetUInt16(0x0004));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.mul_ab.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected06 *= expected03;
				Assert.AreEqual(expected06, vm.GetByte(0x0006));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.mul_as.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected07 *= expected04;
				Assert.AreEqual(expected07, vm.GetUInt16(0x0007));
				
				vm.ExecuteCurrentInstruction();
				expectedIp = 0x10;
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
			}
		}
		
		[Test]
		public void AddModCommandsTest()
		{
			var vm = new MCAVirtualMachine(new byte[]
			{
				0x10,0x00,//Instruction pointer = 0x0010
				0b0100_0100,//IO bitmask
				0x00, 0x00, 0x00, 0x00, 0x00,//reserved
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,//io ports
				(byte) MCAVirtualMachine.Instructions.add_vb,     0x03,0x00,    0x10,
				(byte) MCAVirtualMachine.Instructions.mod_vb,     0x03,0x00,    0x71,
				
				(byte) MCAVirtualMachine.Instructions.add_vs,     0x04,0x00,    0x11,0x00,
				(byte) MCAVirtualMachine.Instructions.mod_vs,     0x04,0x00,    0x13,0x77,
				
				(byte) MCAVirtualMachine.Instructions.add_ab,     0x06,0x00,    0x03,0x00,
				(byte) MCAVirtualMachine.Instructions.mod_ab,     0x06,0x00,    0x03,0x00,
				
				(byte) MCAVirtualMachine.Instructions.add_as,     0x07,0x00,    0x04,0x00,
				(byte) MCAVirtualMachine.Instructions.mod_as,     0x07,0x00,    0x04,0x00,
				
				(byte) MCAVirtualMachine.Instructions.mov_vs,     0x00,0x00,    0x10,0x00,//ip = 0x10 (jump to 0x10)
			});

			byte expected03 = 0;
			ushort expected04 = 0;
			byte expected06 = 0;
			ushort expected07 = 0;

			var expectedIp = 0x10;

			for (int i = 0; i < 0xffff; i++)
			{
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_vb.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected03 += 0x10;
				Assert.AreEqual(expected03, vm.GetByte(0x0003));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.mod_vb.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected03 %= 0x71;
				Assert.AreEqual(expected03, vm.GetByte(0x0003));
				
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_vs.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected04 += 0x0011;
				Assert.AreEqual(expected04, vm.GetUInt16(0x0004));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.mod_vs.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected04 %= 0x7713;
				Assert.AreEqual(expected04, vm.GetUInt16(0x0004));
				
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_ab.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected06 += expected03;
				Assert.AreEqual(expected06, vm.GetByte(0x0006));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.mod_ab.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				if (expected03 != 0) expected06 %= expected03;
				else expected06 = 0;
				Assert.AreEqual(expected06, vm.GetByte(0x0006));
				
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_as.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected07 += expected04;
				Assert.AreEqual(expected07, vm.GetUInt16(0x0007));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.mod_as.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				if (expected04 != 0) expected07 %= expected04;
				else expected07 = 0;
				Assert.AreEqual(expected07, vm.GetUInt16(0x0007));
				
				vm.ExecuteCurrentInstruction();
				expectedIp = 0x10;
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
			}
		}
		
		[Test]
		public void AddOrCommandsTest()
		{
			var vm = new MCAVirtualMachine(new byte[]
			{
				0x10,0x00,//Instruction pointer = 0x0010
				0b0100_0100,//IO bitmask
				0x00, 0x00, 0x00, 0x00, 0x00,//reserved
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,//io ports
				(byte) MCAVirtualMachine.Instructions.add_vb,     0x03,0x00,    0x10,
				(byte) MCAVirtualMachine.Instructions.or_vb,     0x03,0x00,    0b1000_0001,
				
				(byte) MCAVirtualMachine.Instructions.add_vs,     0x04,0x00,    0x11,0x00,
				(byte) MCAVirtualMachine.Instructions.or_vs,     0x04,0x00,    0b0100_0010,0b1000_1001,
				
				(byte) MCAVirtualMachine.Instructions.add_ab,     0x06,0x00,    0x03,0x00,
				(byte) MCAVirtualMachine.Instructions.or_ab,     0x06,0x00,    0x03,0x00,
				
				(byte) MCAVirtualMachine.Instructions.add_as,     0x07,0x00,    0x04,0x00,
				(byte) MCAVirtualMachine.Instructions.or_as,     0x07,0x00,    0x04,0x00,
				
				(byte) MCAVirtualMachine.Instructions.mov_vs,     0x00,0x00,    0x10,0x00,//ip = 0x10 (jump to 0x10)
			});

			byte expected03 = 0;
			ushort expected04 = 0;
			byte expected06 = 0;
			ushort expected07 = 0;

			var expectedIp = 0x10;

			for (int i = 0; i < 0xffff; i++)
			{
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_vb.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected03 += 0x10;
				Assert.AreEqual(expected03, vm.GetByte(0x0003));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.or_vb.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected03 |= 0b1000_0001;
				Assert.AreEqual(expected03, vm.GetByte(0x0003));
				
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_vs.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected04 += 0x0011;
				Assert.AreEqual(expected04, vm.GetUInt16(0x0004));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.or_vs.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected04 |= 0b1000_1001_0100_0010;
				Assert.AreEqual(expected04, vm.GetUInt16(0x0004));
				
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_ab.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected06 += expected03;
				Assert.AreEqual(expected06, vm.GetByte(0x0006));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.or_ab.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected06 |= expected03;
				Assert.AreEqual(expected06, vm.GetByte(0x0006));
				
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_as.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected07 += expected04;
				Assert.AreEqual(expected07, vm.GetUInt16(0x0007));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.or_as.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected07 |= expected04;
				Assert.AreEqual(expected07, vm.GetUInt16(0x0007));
				
				vm.ExecuteCurrentInstruction();
				expectedIp = 0x10;
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
			}
		}
		
		[Test]
		public void AddXorCommandsTest()
		{
			var vm = new MCAVirtualMachine(new byte[]
			{
				0x10,0x00,//Instruction pointer = 0x0010
				0b0100_0100,//IO bitmask
				0x00, 0x00, 0x00, 0x00, 0x00,//reserved
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,//io ports
				(byte) MCAVirtualMachine.Instructions.add_vb,     0x03,0x00,    0x10,
				(byte) MCAVirtualMachine.Instructions.xor_vb,     0x03,0x00,    0b1010_1101,
				
				(byte) MCAVirtualMachine.Instructions.add_vs,     0x04,0x00,    0x11,0x00,
				(byte) MCAVirtualMachine.Instructions.xor_vs,     0x04,0x00,    0b0110_0010,0b1000_1011,
				
				(byte) MCAVirtualMachine.Instructions.add_ab,     0x06,0x00,    0x03,0x00,
				(byte) MCAVirtualMachine.Instructions.xor_ab,     0x06,0x00,    0x03,0x00,
				
				(byte) MCAVirtualMachine.Instructions.add_as,     0x07,0x00,    0x04,0x00,
				(byte) MCAVirtualMachine.Instructions.xor_as,     0x07,0x00,    0x04,0x00,
				
				(byte) MCAVirtualMachine.Instructions.mov_vs,     0x00,0x00,    0x10,0x00,//ip = 0x10 (jump to 0x10)
			});

			byte expected03 = 0;
			ushort expected04 = 0;
			byte expected06 = 0;
			ushort expected07 = 0;

			var expectedIp = 0x10;

			for (int i = 0; i < 0xffff; i++)
			{
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_vb.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected03 += 0x10;
				Assert.AreEqual(expected03, vm.GetByte(0x0003));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.xor_vb.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected03 ^= 0b1010_1101;
				Assert.AreEqual(expected03, vm.GetByte(0x0003));
				
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_vs.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected04 += 0x0011;
				Assert.AreEqual(expected04, vm.GetUInt16(0x0004));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.xor_vs.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected04 ^= 0b1000_1011_0110_0010;
				Assert.AreEqual(expected04, vm.GetUInt16(0x0004));
				
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_ab.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected06 += expected03;
				Assert.AreEqual(expected06, vm.GetByte(0x0006));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.xor_ab.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected06 ^= expected03;
				Assert.AreEqual(expected06, vm.GetByte(0x0006));
				
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_as.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected07 += expected04;
				Assert.AreEqual(expected07, vm.GetUInt16(0x0007));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.xor_as.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected07 ^= expected04;
				Assert.AreEqual(expected07, vm.GetUInt16(0x0007));
				
				vm.ExecuteCurrentInstruction();
				expectedIp = 0x10;
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
			}
		}
		
		[Test]
		public void AddShiftLeftCommandsTest()
		{
			var vm = new MCAVirtualMachine(new byte[]
			{
				0x10,0x00,//Instruction pointer = 0x0010
				0b0100_0100,//IO bitmask
				0x00, 0x00, 0x00, 0x00, 0x00,//reserved
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,//io ports
				(byte) MCAVirtualMachine.Instructions.add_vb,     0x03,0x00,    0b0000_0101,
				(byte) MCAVirtualMachine.Instructions.shl_vb,     0x03,0x00,    0x02,
				
				(byte) MCAVirtualMachine.Instructions.add_vs,     0x04,0x00,    0x11,0x00,
				(byte) MCAVirtualMachine.Instructions.shl_vs,     0x04,0x00,    0x03,0x00,
				
				(byte) MCAVirtualMachine.Instructions.add_ab,     0x06,0x00,    0x03,0x00,
				(byte) MCAVirtualMachine.Instructions.shl_ab,     0x06,0x00,    0x03,0x00,
				
				(byte) MCAVirtualMachine.Instructions.add_as,     0x07,0x00,    0x04,0x00,
				(byte) MCAVirtualMachine.Instructions.shl_as,     0x07,0x00,    0x04,0x00,
				
				(byte) MCAVirtualMachine.Instructions.mov_vs,     0x00,0x00,    0x10,0x00,//ip = 0x10 (jump to 0x10)
			});

			byte expected03 = 0;
			ushort expected04 = 0;
			byte expected06 = 0;
			ushort expected07 = 0;

			var expectedIp = 0x10;

			for (int i = 0; i < 0xffff; i++)
			{
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_vb.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected03 += 0b0000_0101;
				Assert.AreEqual(expected03, vm.GetByte(0x0003));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.shl_vb.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected03 <<= 0x02;
				Assert.AreEqual(expected03, vm.GetByte(0x0003));
				
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_vs.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected04 += 0x0011;
				Assert.AreEqual(expected04, vm.GetUInt16(0x0004));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.shl_vs.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected04 <<= 0x0003;
				Assert.AreEqual(expected04, vm.GetUInt16(0x0004));
				
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_ab.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected06 += expected03;
				Assert.AreEqual(expected06, vm.GetByte(0x0006));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.shl_ab.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected06 <<= expected03;
				Assert.AreEqual(expected06, vm.GetByte(0x0006));
				
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_as.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected07 += expected04;
				Assert.AreEqual(expected07, vm.GetUInt16(0x0007));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.shl_as.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected07 <<= expected04;
				Assert.AreEqual(expected07, vm.GetUInt16(0x0007));
				
				vm.ExecuteCurrentInstruction();
				expectedIp = 0x10;
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
			}
		}
		
		[Test]
		public void AddShiftRightCommandsTest()
		{
			var vm = new MCAVirtualMachine(new byte[]
			{
				0x10,0x00,//Instruction pointer = 0x0010
				0b0100_0100,//IO bitmask
				0x00, 0x00, 0x00, 0x00, 0x00,//reserved
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,//io ports
				(byte) MCAVirtualMachine.Instructions.add_vb,     0x03,0x00,    0b1001_1101,
				(byte) MCAVirtualMachine.Instructions.shr_vb,     0x03,0x00,    0x02,
				
				(byte) MCAVirtualMachine.Instructions.add_vs,     0x04,0x00,    0x11,0x00,
				(byte) MCAVirtualMachine.Instructions.shr_vs,     0x04,0x00,    0x03,0x00,
				
				(byte) MCAVirtualMachine.Instructions.add_ab,     0x06,0x00,    0x03,0x00,
				(byte) MCAVirtualMachine.Instructions.shr_ab,     0x06,0x00,    0x03,0x00,
				
				(byte) MCAVirtualMachine.Instructions.add_as,     0x07,0x00,    0x04,0x00,
				(byte) MCAVirtualMachine.Instructions.shr_as,     0x07,0x00,    0x04,0x00,
				
				(byte) MCAVirtualMachine.Instructions.mov_vs,     0x00,0x00,    0x10,0x00,//ip = 0x10 (jump to 0x10)
			});

			byte expected03 = 0;
			ushort expected04 = 0;
			byte expected06 = 0;
			ushort expected07 = 0;

			var expectedIp = 0x10;

			for (int i = 0; i < 0xffff; i++)
			{
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_vb.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected03 += 0b1001_1101;
				Assert.AreEqual(expected03, vm.GetByte(0x0003));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.shr_vb.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected03 >>= 0x02;
				Assert.AreEqual(expected03, vm.GetByte(0x0003));
				
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_vs.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected04 += 0x0011;
				Assert.AreEqual(expected04, vm.GetUInt16(0x0004));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.shr_vs.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected04 >>= 0x0003;
				Assert.AreEqual(expected04, vm.GetUInt16(0x0004));
				
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_ab.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected06 += expected03;
				Assert.AreEqual(expected06, vm.GetByte(0x0006));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.shr_ab.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected06 >>= expected03;
				Assert.AreEqual(expected06, vm.GetByte(0x0006));
				
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_as.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected07 += expected04;
				Assert.AreEqual(expected07, vm.GetUInt16(0x0007));
				
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.shr_as.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected07 >>= expected04;
				Assert.AreEqual(expected07, vm.GetUInt16(0x0007));
				
				vm.ExecuteCurrentInstruction();
				expectedIp = 0x10;
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
			}
		}
		
		[Test]
		public void AddJumpLessCommandsTest()
		{
			var vm = new MCAVirtualMachine(new byte[]
			{
				0x10,0x00,//Instruction pointer = 0x0010
				0b0100_0100,//IO bitmask
				0x00, 0x00, 0x00, 0x00, 0x00,//reserved
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,//io ports
				(byte) MCAVirtualMachine.Instructions.add_vb,     0x03,0x00,    0x17,
				(byte) MCAVirtualMachine.Instructions.jl_vb,      0x03,0x00,    0xc1,           0x10,0x00,
				
				(byte) MCAVirtualMachine.Instructions.add_vs,     0x04,0x00,    0xf7,0x01,
				(byte) MCAVirtualMachine.Instructions.jl_vs,      0x04,0x00,    0x0f,0xf0,      0x1a,0x00,
				
				(byte) MCAVirtualMachine.Instructions.add_ab,     0x06,0x00,    0x03,0x00,
				(byte) MCAVirtualMachine.Instructions.jl_ab,      0x06,0x00,    0x03,0x00,      0x26,0x00,
				
				(byte) MCAVirtualMachine.Instructions.add_as,     0x07,0x00,    0x04,0x00,
				(byte) MCAVirtualMachine.Instructions.jl_as,      0x07,0x00,    0x04,0x00,      0x32,0x00
			});

			byte expected03 = 0;
			ushort expected04 = 0;
			byte expected06 = 0;
			ushort expected07 = 0;

			var expectedIp = 0x10;

			while (true)
			{
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_vb.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected03 += 0x17;
				Assert.AreEqual(expected03, vm.GetByte(0x03));
				
				vm.ExecuteCurrentInstruction();
				if (expected03 < 0xc1)
				{
					expectedIp = 0x10;
					Assert.AreEqual(expectedIp, vm.InstructionPointer);
				}
				else
				{
					expectedIp += MCAVirtualMachine.Instructions.jl_vb.GetLength();
					Assert.AreEqual(expectedIp, vm.InstructionPointer);
					break;
				}
			}
			
			while (true)
			{
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_vs.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected04 += 0x01f7;
				Assert.AreEqual(expected04, vm.GetUInt16(0x04));
				
				vm.ExecuteCurrentInstruction();
				if (expected04 < 0xf00f)
				{
					expectedIp = 0x1a;
					Assert.AreEqual(expectedIp, vm.InstructionPointer);
				}
				else
				{
					expectedIp += MCAVirtualMachine.Instructions.jl_vs.GetLength();
					Assert.AreEqual(expectedIp, vm.InstructionPointer);
					break;
				}
			}
			
			while (true)
			{
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_ab.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected06 += expected03;
				Assert.AreEqual(expected06, vm.GetByte(0x06));
				
				vm.ExecuteCurrentInstruction();
				if (expected06 < expected03)
				{
					expectedIp = 0x26;
					Assert.AreEqual(expectedIp, vm.InstructionPointer);
				}
				else
				{
					expectedIp += MCAVirtualMachine.Instructions.jl_ab.GetLength();
					Assert.AreEqual(expectedIp, vm.InstructionPointer);
					break;
				}
			}
			
			while (true)
			{
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.add_as.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected07 += expected04;
				Assert.AreEqual(expected07, vm.GetUInt16(0x07));
				
				vm.ExecuteCurrentInstruction();
				if (expected07 < expected04)
				{
					expectedIp = 0x32;
					Assert.AreEqual(expectedIp, vm.InstructionPointer);
				}
				else
				{
					expectedIp += MCAVirtualMachine.Instructions.jl_as.GetLength();
					Assert.AreEqual(expectedIp, vm.InstructionPointer);
					break;
				}
			}
		}
		
		[Test]
		public void SubJumpGreaterCommandsTest()
		{
			var vm = new MCAVirtualMachine(new byte[]
			{
				0x10,0x00,//Instruction pointer = 0x0010
				0b0100_0100,//IO bitmask
				0x00, 0x00, 0x00, 0x00, 0x00,//reserved
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,//io ports
				(byte) MCAVirtualMachine.Instructions.sub_vb,     0x03,0x00,    0x17,
				(byte) MCAVirtualMachine.Instructions.jg_vb,      0x03,0x00,    0x20,           0x10,0x00,
				
				(byte) MCAVirtualMachine.Instructions.sub_vs,     0x04,0x00,    0x01,0x01,
				(byte) MCAVirtualMachine.Instructions.jg_vs,      0x04,0x00,    0x02,0x02,      0x1a,0x00,
				
				(byte) MCAVirtualMachine.Instructions.sub_ab,     0x06,0x00,    0x03,0x00,
				(byte) MCAVirtualMachine.Instructions.jg_ab,      0x06,0x00,    0x03,0x00,      0x26,0x00,
				
				(byte) MCAVirtualMachine.Instructions.sub_as,     0x07,0x00,    0x04,0x00,
				(byte) MCAVirtualMachine.Instructions.jg_as,      0x07,0x00,    0x04,0x00,      0x32,0x00
			});

			byte expected03 = 0;
			ushort expected04 = 0;
			byte expected06 = 0;
			ushort expected07 = 0;

			var expectedIp = 0x10;

			while (true)
			{
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.sub_vb.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected03 -= 0x17;
				Assert.AreEqual(expected03, vm.GetByte(0x03));
				
				vm.ExecuteCurrentInstruction();
				if (expected03 > 0x20)
				{
					expectedIp = 0x10;
					Assert.AreEqual(expectedIp, vm.InstructionPointer);
				}
				else
				{
					expectedIp += MCAVirtualMachine.Instructions.jg_vb.GetLength();
					Assert.AreEqual(expectedIp, vm.InstructionPointer);
					break;
				}
			}
			
			while (true)
			{
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.sub_vs.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected04 -= 0x0101;
				Assert.AreEqual(expected04, vm.GetUInt16(0x04));
				
				vm.ExecuteCurrentInstruction();
				if (expected04 > 0x0202)
				{
					expectedIp = 0x1a;
					Assert.AreEqual(expectedIp, vm.InstructionPointer);
				}
				else
				{
					expectedIp += MCAVirtualMachine.Instructions.jg_vs.GetLength();
					Assert.AreEqual(expectedIp, vm.InstructionPointer);
					break;
				}
			}
			
			while (true)
			{
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.sub_ab.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected06 -= expected03;
				Assert.AreEqual(expected06, vm.GetByte(0x06));
				
				vm.ExecuteCurrentInstruction();
				if (expected06 > expected03)
				{
					expectedIp = 0x26;
					Assert.AreEqual(expectedIp, vm.InstructionPointer);
				}
				else
				{
					expectedIp += MCAVirtualMachine.Instructions.jg_ab.GetLength();
					Assert.AreEqual(expectedIp, vm.InstructionPointer);
					break;
				}
			}
			
			while (true)
			{
				vm.ExecuteCurrentInstruction();
				expectedIp += MCAVirtualMachine.Instructions.sub_as.GetLength();
				Assert.AreEqual(expectedIp, vm.InstructionPointer);
				expected07 -= expected04;
				Assert.AreEqual(expected07, vm.GetUInt16(0x07));
				
				vm.ExecuteCurrentInstruction();
				if (expected07 > expected04)
				{
					expectedIp = 0x32;
					Assert.AreEqual(expectedIp, vm.InstructionPointer);
				}
				else
				{
					expectedIp += MCAVirtualMachine.Instructions.jg_as.GetLength();
					Assert.AreEqual(expectedIp, vm.InstructionPointer);
					break;
				}
			}
		}
	}
}