﻿using System;
using System.Windows.Forms;
using OrbitalShipHacker.Engine;
using OrbitalShipHacker.Graphics;
using OrbitalShipHacker.VM;

namespace OrbitalShipHacker
{
	static class Program
	{
		[STAThread]
		public static void Main(string[] args)
		{
			var board = new CircuitPlane(new Vector2i(64, 64));
			for (var x = 0; x < board.Size.X; x++)
			for (var y = 0; y < board.Size.Y; y++)
				board.Blocks[x, y] = Blocks.Circuit;

			var player = new Player();
			player.CreativeMode = true;
			
			var form = new CircuitForm(new Level(player, board));
			Application.Run(form);
		}
	}
}