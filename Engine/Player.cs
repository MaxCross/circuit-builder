﻿using System.Collections.Generic;
using System.IO;

namespace OrbitalShipHacker.Engine
{
	public class Player
	{
		public bool CreativeMode;
		public List<IItem> Items = new List<IItem>();

		public bool UseItem(CircuitPlane board, Level level, int slot, Vector2i point, int actionId)
		{
			if (slot >= Items.Count) return false;
			var result = Items[slot].Use(this, board, level, point, actionId);
			if (!result) return false;
			if(Items[slot].ShouldBeRemovedFromInventory)
				Items.RemoveAt(slot);
			return true;
		}

		public IItem GetItem(int slot) => slot >= Items.Count || slot < 0 ? null : Items[slot];

		public int AddItem(IItem item)
		{
			Items.Add(item);
			return Items.Count - 1;
		}

		public static Player Load(BinaryReader reader)
		{
			var result = new Player();
			
			var itemsCount = reader.ReadInt32();
			for (var i = 0; i < itemsCount; i++) result.Items.Add(ItemExtensions.LoadItem(reader));

			return result;
		}

		public void Save(BinaryWriter writer)
		{
			writer.Write(Items.Count);
			foreach (var item in Items) item.SaveItem(writer);
		}
	}
}