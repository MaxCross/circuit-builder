﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using OrbitalShipHacker.VM;

namespace OrbitalShipHacker.Engine
{
	public class MCA : ICircuitElement
	{
		public readonly MCAVirtualMachine VirtualMachine;
		// public CircuitBoard Circuit { get; set; }
		public Vector2i Size => new Vector2i(7, 11);

		public ICircuitElement Copy()
		{
			var data = new byte[VirtualMachine.DataLength];
			VirtualMachine.Data.CopyTo(data, 0);

			return new MCA(data);
		}

		public MCA(ushort dataLength = 0xffff) => VirtualMachine = new MCAVirtualMachine(dataLength);
		public MCA(byte[] data) => VirtualMachine = new MCAVirtualMachine(data);

		public enum Ports : byte
		{
			io0,
			io1,
			io2,
			io3,
			io4,
			io5,
			io6,
			io7,
			io8,
			
			clock = 11
		}

		public IEnumerable<Vector2i> ConnectionLocalPoints
		{
			get
			{
				yield return new Vector2i(2, 0);
				yield return new Vector2i(4, 0);
				
				yield return new Vector2i(0, 2);
				yield return new Vector2i(0, 4);
				yield return new Vector2i(0, 6);
				yield return new Vector2i(0, 8);
				
				yield return new Vector2i(6, 2);
				yield return new Vector2i(6, 4);
				yield return new Vector2i(6, 6);
				yield return new Vector2i(6, 8);
				
				yield return new Vector2i(2, 10);
				yield return new Vector2i(4, 10);
			}
		}

		public bool IsOccupiing(Vector2i localPoint)
		{
			if (localPoint.X >= 1 && localPoint.X <= 5 && localPoint.Y >= 1 && localPoint.Y <= 9) return true;
			if (ConnectionLocalPoints.Contains(localPoint)) return true;
			return false;
		}

		public IEnumerable<Vector2i> OccupiingLocalPoints
		{
			get
			{
				foreach (var connectionPoint in ConnectionLocalPoints) yield return connectionPoint;
				for (var x = 1; x <= 5; x++)
				for (var y = 1; y <= 9; y++)
					yield return new Vector2i(x, y);
			}
		}
		
		public Vector2i GetPortLocalPosition(byte n)
		{
			if(n < 8)
				return new Vector2i(6 * (n % 2), 2 + 2 * (n / 2));
			
			n -= 8;
			return new Vector2i( 2 + 2 * (n % 2), 10 * (n / 2));
		}

		public void AcceptInputs(CircuitPlane circuit, Vector2i position, Rotations rotation)
		{
			if(VirtualMachine.GetByte(3) != 0 || GetInput((byte) Ports.clock) != 0xff)
				return;
			
			var ioMask = VirtualMachine.GetByte(2);
			
			for (byte i = 0; i < 8; i++)
				if (((ioMask >> i) & 1) == 0)
					VirtualMachine.SetByte((ushort) (8 + i), GetInput(i));
			
			byte GetInput(byte port)
				=> circuit.GetInputSignal(this.LocalToWorld(GetPortLocalPosition(port), position, rotation));
		}
		
		public void SendOutputs(CircuitPlane circuit, Vector2i position, Rotations rotation)
		{
			var ioMask = VirtualMachine.GetByte(2);
			
			for (byte i = 0; i < 8; i++)
				if (((ioMask >> i) & 1) == 1)
					SetOutput(i, VirtualMachine.GetByte((ushort) (8 + i)));

			void SetOutput(byte port, byte output)
				=> circuit.SetOutputSignal(this.LocalToWorld(GetPortLocalPosition(port), position, rotation), output);
		}

		public void Tick(CircuitPlane circuit, Vector2i position, Rotations rotation)
		{
			var clockInput = GetInput((byte) Ports.clock);
			if(VirtualMachine.GetByte(3) == 0 && clockInput == 0xff)
				VirtualMachine.ExecuteCurrentInstruction();
			VirtualMachine.SetByte(3, clockInput);
			
			byte GetInput(byte port)
				=> circuit.GetInputSignal(this.LocalToWorld(GetPortLocalPosition(port), position, rotation));
		}

		public static MCA Load(BinaryReader reader)
		{
			var length = reader.ReadUInt16();
			var data = new byte[length];
			reader.Read(data, 0, length);
			return new MCA(data);
		}

		public void Save(BinaryWriter writer)
		{
			writer.Write(VirtualMachine.DataLength);
			writer.Write(VirtualMachine.Data);
		}
	}
}