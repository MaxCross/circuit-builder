﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace OrbitalShipHacker.Engine
{
	public interface IItem
	{
		public string Name { get; }
		public string FullName { get; }
		public bool Use(Player player, CircuitPlane board, Level level, Vector2i point, int actionId = 0);
		public bool ShouldBeRemovedFromInventory { get; }
		IItem Copy();
	}

	public enum Items : byte
	{
		Null,
		PlacableElement,
		WirePrinter,
		AccessEditor
	}

	public static class ItemExtensions
	{
		public static IItem LoadItem(BinaryReader reader) => (Items) reader.ReadByte() switch
		{
			Items.Null => null,
			Items.PlacableElement => new PlacableElement(CircuitElementExtensions.Load(reader)),
			Items.WirePrinter => new WirePrinter(reader.ReadInt32()),
			Items.AccessEditor => new AccessEditor(),
			_ => null
		};

		public static void SaveItem(this IItem item, BinaryWriter writer)
		{
			switch (item)
			{
				case null:
					writer.Write((byte) Items.Null);
					return;
				case PlacableElement pe:
					writer.Write((byte) Items.PlacableElement);
					pe.Element.Save(writer);
					return;
				case WirePrinter printer:
					writer.Write((byte) Items.WirePrinter);
					writer.Write(printer.WiresLeft);
					return;
				case AccessEditor _:
					writer.Write((byte) Items.AccessEditor);
					break;
				default: return;
			}
		}
	}

	public class PlacableElement : IItem
	{
		public string Name => FullName;

		public string FullName => Element switch
		{
			MCA _ => "MCA",
			MagicClock _ => "Magic ticker",
			Indicator _ => "Indicator",
			LevelTarget _ => "Level target",
			_ => "UNDEFINED FULL NAME!!"
		};
		public readonly ICircuitElement Element;
		public Rotations targetRotation;

		public IItem Copy() => new PlacableElement(Element.Copy());

		public PlacableElement(ICircuitElement element) => Element = element;

		public bool Use(Player player, CircuitPlane board, Level level, Vector2i point, int actionId)
		{
			if (board == null) return false;
			if (actionId == 0)
			{
				var size = targetRotation == Rotations.Up || targetRotation == Rotations.Down
					? Element.Size
					: new Vector2i(Element.Size.Y, Element.Size.X);
				if (!player.CreativeMode &&
				    (!level.AllowedArea.Contains(point)
				     || (point + size).X > level.AllowedArea.Right
				     || (point + size).Y > level.AllowedArea.Bottom)) return false;
				if (!board.AddElement(Element, point, targetRotation)) return false;
				ShouldBeRemovedFromInventory = true;
				return true;
			}

			if (actionId == 4)
			{
				targetRotation = targetRotation switch
				{
					Rotations.Up => Rotations.Right,
					Rotations.Right => Rotations.Down,
					Rotations.Down => Rotations.Left,
					Rotations.Left => Rotations.Up,
					_ => Rotations.Up
				};
				return true;
			}
			if (actionId == 5)
			{
				targetRotation = targetRotation switch
				{
					Rotations.Up => Rotations.Left,
					Rotations.Right => Rotations.Up,
					Rotations.Down => Rotations.Right,
					Rotations.Left => Rotations.Down,
					_ => Rotations.Up
				};
				return true;
			}

			return false;
		}

		public bool ShouldBeRemovedFromInventory { get; private set; }
	}

	public class WirePrinter : IItem
	{
		public string Name => "Wire Printer";
		public string FullName => $"Wire Printer x{WiresLeft}";
		public int WiresLeft;
		public Vector2i LastPoint = new Vector2i(-1, -1);

		public IItem Copy() => new WirePrinter(WiresLeft);

		public WirePrinter(int wiresCount = 256)
		{
			WiresLeft = wiresCount;
		}
		public bool Use(Player player, CircuitPlane board, Level level, Vector2i point, int actionId)
		{
			if (board == null || !board.IsPointOnCircuit(point) || board.GetElementAt(point).Element != null) return false;

			if (actionId == 0)
			{
				LastPoint = board.IsPointOnCircuit(point) ? point : new Vector2i(-1, -1);
				return true;
			}

			if (actionId == 1)
			{
				LastPoint = new Vector2i(-1, -1);
				return true;
			}

			if (actionId == 2)
			{
				if (point == LastPoint)
				{
					if (!player.CreativeMode && !level.AllowedArea.Contains(point)) return false;
					var targetType = board.SignalGrid[point.X, point.Y].Type switch
					{
						WireType.Empty => WireType.Normal,
						WireType.Normal => WireType.CrossBrige,
						WireType.CrossBrige => WireType.Empty,
						_ => WireType.Empty
					};
					LastPoint = new Vector2i(-1, -1);
					return board.SetWire(point, targetType);
				}

				foreach (var p in LastPoint.LineTo(point))
				{
					if((!player.CreativeMode && !level.AllowedArea.Contains(p)) ||
					   WiresLeft <= 0 || !board.IsPointOnCircuit(p) || board.GetElementAt(p).Element != null) break;
					
					if (board.SignalGrid[p.X, p.Y].Type == WireType.Empty && board.SetWire(p, WireType.Normal))
						WiresLeft--;
				}

				LastPoint = new Vector2i(-1, -1);
				return true;
			}

			if (actionId == 7)
			{
				if (!player.CreativeMode && !level.AllowedArea.Contains(point)) return false;
				if (board.SignalGrid[point.X, point.Y].Type != WireType.Empty && board.SetWire(point, WireType.Empty))
					WiresLeft++;
			}

			return false;
		}

		public bool ShouldBeRemovedFromInventory => false;
	}

	public class AccessEditor : IItem
	{
		public string Name => "Access Editor";
		public string FullName => Name;

		public IItem Copy() => new AccessEditor();

		public bool Use(Player player, CircuitPlane board, Level level, Vector2i point, int actionId = 0)
		{
			if (actionId == 0)
			{
				if(!board.IsPointOnCircuit(point)) return false;
				var i = board.GetElementIndexAt(point);
				if (i == -1) return false;
				level.DisallowedElements[i] = !level.DisallowedElements[i];
				return true;
			}

			if (actionId == 1) level.AllowedArea.Location = point;
			if (actionId == 3)
			{
				level.AllowedArea.Size = point - level.AllowedArea.Location;
				if (level.AllowedArea.Size.Width < 0)
				{
					level.AllowedArea.Location = new Point(
						level.AllowedArea.Location.X + level.AllowedArea.Size.Width,
						level.AllowedArea.Location.Y);
					level.AllowedArea.Size = new Size(
						-level.AllowedArea.Size.Width, level.AllowedArea.Size.Height);
				}
				if (level.AllowedArea.Size.Height < 0)
				{
					level.AllowedArea.Location = new Point(
						level.AllowedArea.Location.X,
						level.AllowedArea.Location.Y + level.AllowedArea.Size.Height);
					level.AllowedArea.Size = new Size(
						level.AllowedArea.Size.Width, -level.AllowedArea.Size.Height);
				}
				if(level.AllowedArea.Size == Size.Empty)
					level.AllowedArea = Rectangle.Empty;
			}

			return true;
		}

		public bool ShouldBeRemovedFromInventory => false;
	}
}