﻿using System;
using System.Collections.Generic;
using System.IO;

namespace OrbitalShipHacker.Engine
{
	public class MagicClock : ICircuitElement
	{
		public Vector2i Size => new Vector2i(3, 4);
		public IEnumerable<Vector2i> ConnectionLocalPoints { get { yield return new Vector2i(1, 3);} }
		public bool PrevState;
		public MagicClock(bool prevState = false) => PrevState = prevState;
		public ICircuitElement Copy() => new MagicClock(PrevState);

		public IEnumerable<Vector2i> OccupiingLocalPoints
		{
			get
			{
				for (var x = 0; x < 3; x++)
				for (var y = 0; y < 3; y++)
					yield return new Vector2i(x, y);
				yield return new Vector2i(1, 3);
			}
		}

		public bool IsOccupiing(Vector2i localPoint)
		{
			return (localPoint.X >= 0 && localPoint.X <= 2 && localPoint.Y >= 0 && localPoint.Y <= 2)
			       || localPoint == new Vector2i(1, 3);
		}
		
		public void AcceptInputs(CircuitPlane circuit, Vector2i position, Rotations rotation) { }

		public void SendOutputs(CircuitPlane circuit, Vector2i position, Rotations rotation)
		{
			circuit.SetOutputSignal(this.LocalToWorld(new Vector2i(1, 3), position, rotation),
				(byte) (PrevState ? 0 : 0xff));
			PrevState = !PrevState;
		}

		public void Tick(CircuitPlane circuit, Vector2i position, Rotations rotation) { }

		public static MagicClock Load(BinaryReader reader) => new MagicClock(reader.ReadBoolean());
		public void Save(BinaryWriter writer) => writer.Write(PrevState);
	}
	
	public class LevelTarget : ICircuitElement
	{
		public Vector2i Size => new Vector2i(3, 4);
		public IEnumerable<Vector2i> ConnectionLocalPoints { get { yield return new Vector2i(1, 3);} }
		public Level Level;
		public ICircuitElement Copy() => new LevelTarget(Level);

		public LevelTarget(Level level = null) => Level = level;

		public IEnumerable<Vector2i> OccupiingLocalPoints
		{
			get
			{
				for (var x = 0; x < 3; x++)
				for (var y = 0; y < 3; y++)
					yield return new Vector2i(x, y);
				yield return new Vector2i(1, 3);
			}
		}

		public bool IsOccupiing(Vector2i localPoint)
		{
			return (localPoint.X >= 0 && localPoint.X <= 2 && localPoint.Y >= 0 && localPoint.Y <= 2)
			       || localPoint == new Vector2i(1, 3);
		}

		public void AcceptInputs(CircuitPlane circuit, Vector2i position, Rotations rotation)
		{
			if (circuit.GetInputSignal(this.LocalToWorld(new Vector2i(1, 3), position, rotation)) == 0xff)
				Level.Pass();
		}

		public void SendOutputs(CircuitPlane circuit, Vector2i position, Rotations rotation) { }

		public void Tick(CircuitPlane circuit, Vector2i position, Rotations rotation) { }
	}

	public class Indicator : ICircuitElement
	{
		public Vector2i Size => new Vector2i(3, 6);
		public IEnumerable<Vector2i> ConnectionLocalPoints { get { yield return new Vector2i(1, 5);} }
		public ICircuitElement Copy() => new Indicator();

		public IEnumerable<Vector2i> OccupiingLocalPoints
		{
			get
			{
				for (var x = 0; x < 3; x++)
				for (var y = 0; y < 5; y++)
					yield return new Vector2i(x, y);
				yield return new Vector2i(1, 5);
			}
		}
		
		public bool IsOccupiing(Vector2i localPoint)
		{
			return (localPoint.X >= 0 && localPoint.X <= 2 && localPoint.Y >= 0 && localPoint.Y <= 4)
			       || localPoint == new Vector2i(1, 5);
		}

		public void AcceptInputs(CircuitPlane circuit, Vector2i position, Rotations rotation)
			=> state = circuit.GetInputSignal(this.LocalToWorld(new Vector2i(1, 5), position, rotation));

		public void SendOutputs(CircuitPlane circuit, Vector2i position, Rotations rotation) { }

		public byte state;
		public void Tick(CircuitPlane circuit, Vector2i position, Rotations rotation) { }
	}
}