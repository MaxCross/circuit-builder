﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace OrbitalShipHacker.Engine
{
	public struct ElementOnCircuitPlane
	{
		public Vector2i Position;
		public Rotations Rotation;
		public ICircuitElement Element;

		public Vector2i LocalToWorld(Vector2i point) => Position + Element.RotateLocalPoint(point, Rotation);
		public Vector2i WorldToLocal(Vector2i point) => Element.ReverseRotateLocalPoint(point - Position, Rotation);

		public void AcceptInputs(CircuitPlane circuit) => Element.AcceptInputs(circuit, Position, Rotation);
		public void Tick(CircuitPlane circuit) => Element.Tick(circuit, Position, Rotation);
		public void SendOutputs(CircuitPlane circuit) => Element.SendOutputs(circuit, Position, Rotation);

		public bool IsOccupiing(Vector2i point) => Element.IsOccupiing(WorldToLocal(point));

		public bool IntersectWitch(ElementOnCircuitPlane other)
		{
			foreach (var localPoint in other.Element.OccupiingLocalPoints)
				if (IsOccupiing(other.LocalToWorld(localPoint)))
					return true;

			return false;
		}

		public bool IntersectWith(ICircuitElement other, Vector2i position, Rotations rotations)//Temp
			=> IntersectWitch(new ElementOnCircuitPlane {Element = other, Position = position, Rotation = rotations});

		public static ElementOnCircuitPlane Load(BinaryReader reader)
		=> new ElementOnCircuitPlane
		{
			Position = new Vector2i(reader.ReadInt32(), reader.ReadInt32()),
			Rotation = (Rotations) reader.ReadByte(),
			Element = CircuitElementExtensions.Load(reader)
		};

		public void Save(BinaryWriter writer)
		{
			writer.Write(Position.X);
			writer.Write(Position.Y);
			
			writer.Write((byte) Rotation);
			
			Element.Save(writer);
		}
	}
	
	public class CircuitPlane
	{
		public readonly Vector2i Size;

		public Blocks[,] Blocks;
		public WiringData[,] SignalGrid;

		public WireNetwork[] Networks;
		public ElementOnCircuitPlane[] Elements;
		
		public const short MaxElements = 256;
		public const short MaxNetworks = 256;

		public CircuitPlane(Vector2i size)
		{
			Size = size;
			Blocks = new Blocks[size.X, size.Y];
			SignalGrid = new WiringData[size.X, size.Y];

			Networks = new WireNetwork[MaxNetworks];
			Elements = new ElementOnCircuitPlane[MaxElements];
		}

		public void Tick()
		{
			foreach (var element in Elements.Where(e => e.Element != null)) element.AcceptInputs(this);
			foreach (var element in Elements.Where(e => e.Element != null)) element.Tick(this);
			foreach (var element in Elements.Where(e => e.Element != null)) element.SendOutputs(this);
			
			foreach (var network in Networks.Where(n => n != null)) network.SetCurrentSignal();
		}

		public void SetOutputSignal(Vector2i point, byte signal)
		{
			if (!IsPointOnCircuit(point)
			    || SignalGrid[point.X, point.Y].Type != WireType.Normal
			    || GetNetwork(point) == null)
				return;
			GetNetwork(point).RequestSignal(signal);
		}

		public byte GetInputSignal(Vector2i point)
		{
			if (!IsPointOnCircuit(point)
			    || SignalGrid[point.X, point.Y].Type != WireType.Normal
			    || GetNetwork(point) == null)
				return 0;
			return GetNetwork(point).Signal;
		}

		// public bool HasIntersections(ICircuitElement element)
		// 	=> Elements.Where(e => e.Element != null && e.Element != element).Any(element.IntersectWith);
		
		public bool HasIntersections(ICircuitElement element, Vector2i position, Rotations rotation)
			=> Elements
				.Where(e => e.Element != null && e.Element != element)
				.Any(e => e.IntersectWith(element, position, rotation));

		public bool AddElement(ICircuitElement element, Vector2i position, Rotations rotation)
		{
			if (!element.IsFitOnCircuit(this, position, rotation) || HasIntersections(element, position, rotation))
				return false;
			
			for (var i = 0; i < Elements.Length; i++)
				if (Elements[i].Element == null)
				{
					Elements[i].Element = element;
					Elements[i].Position = position;
					Elements[i].Rotation = rotation;
					return true;
				}

			return false;
		}

		public ElementOnCircuitPlane GetElementAt(Vector2i position)
			=> Elements.Where(e => e.Element != null)
				.FirstOrDefault(e => e.IsOccupiing(position));

		public int GetElementIndexAt(Vector2i position)
		{
			for (var i = 0; i < Elements.Length; i++)
				if (Elements[i].Element != null && Elements[i].IsOccupiing(position)) return i;
			return -1;
		}

		public bool RemoveElement(int id)
		{
			if (id >= Elements.Length)
				return false;
			
			Elements[id].Element = null;
			return true;
		}
		
		public bool RemoveElement(Vector2i position)
		{
			for (var i = 0; i < Elements.Length; i++)
				if (Elements[i].IsOccupiing(position))
					return RemoveElement(i);
			return false;
		}

		public bool RemoveElement(ICircuitElement element)
		{
			for (var i = 0; i < Elements.Length; i++)
				if (Elements[i].Element == element)
					return RemoveElement(i);
			return false;
		}

		public bool SetWire(Vector2i position, WireType type)
		{
			if (Blocks[position.X, position.Y] != Engine.Blocks.Circuit)
				return false;
			if (SignalGrid[position.X, position.Y].Type == type)
				return true;
			
			SignalGrid[position.X, position.Y].Type = type;
			
			var up = SkipBriges(position + Vector2i.Up, Vector2i.Up);
			var down = SkipBriges(position + Vector2i.Down, Vector2i.Down);
			var left = SkipBriges(position + Vector2i.Left, Vector2i.Left);
			var right = SkipBriges(position + Vector2i.Right, Vector2i.Right);

			if (type == WireType.Normal)
			{
				SetNetworkId(position, -1);
				var ids = new[] {up, down, left, right}.Select(GetNetworkId).Where(id => id != -1).ToArray();

				if (ids.Length > 0)
				{
					var id = ids.Min();
					SetNetwork(position, id, true);
					return true;
				}

				for (short i = 0; i < Networks.Length; i++)
				{
					if (Networks[i] != null) continue;

					Networks[i] = new WireNetwork();
					SetNetwork(position, i, true);
					break;
				}
				return true;
			}

			

			if (type == WireType.CrossBrige)
			{
				if(GetNetworkId(up) <= GetNetworkId(down) && GetNetworkId(up) != -1)
					SetNetwork(down, GetNetworkId(up));
				else
					SetNetwork(up, GetNetworkId(down));
				
				if(GetNetworkId(left) <= GetNetworkId(right) && GetNetworkId(left) != -1)
					SetNetwork(right, GetNetworkId(left));
				else
					SetNetwork(left, GetNetworkId(right));
				
				SplitNetwork(up, left);
				SplitNetwork(up, right);
				
				SplitNetwork(down, left);
				SplitNetwork(down, right);
			}

			if (type == WireType.Empty)
			{
				if(GetNetworkId(up) == -1 && GetNetworkId(down) == -1 &&
				   GetNetworkId(left) == -1 && GetNetworkId(right) == -1)
				{
					var nid = SignalGrid[position.X, position.Y].NetworkId;
					if (nid >= 0) Networks[nid] = null;
					return true;
				}
				
				SplitNetwork(up, down);
				SplitNetwork(left, right);
				
				SplitNetwork(up, left);
				SplitNetwork(up, right);
				
				SplitNetwork(down, left);
				SplitNetwork(down, right);
			}

			return true;
		}

		public short GetNetworkId(Vector2i point)
		{
			if (!IsPointOnCircuit(point) || SignalGrid[point.X, point.Y].Type != WireType.Normal)
				return -1;
			return SignalGrid[point.X, point.Y].NetworkId;
		}

		public void SetNetworkId(Vector2i point, short newId)
		{
			if (!IsPointOnCircuit(point) || SignalGrid[point.X, point.Y].Type != WireType.Normal)
				return;
			SignalGrid[point.X, point.Y].NetworkId = newId;
		}

		public WireNetwork GetNetwork(Vector2i point)
		{
			var id = GetNetworkId(point);
			return (id >= 0 && id < Networks.Length) ? Networks[id] : null;
		}

		public IEnumerable<Vector2i> EnumerateNetwork(Vector2i start, int ignoreId = -1)
		{
			if (SignalGrid[start.X, start.Y].Type == WireType.Empty)
				yield break;

			// var startNetId = GetNetworkId(start);
			
			var visited = new HashSet<Vector2i>();
			var queue = new Queue<Vector2i>();
			queue.Enqueue(start);

			while (queue.Count > 0)
			{
				var pos = queue.Dequeue();
				visited.Add(pos);
				yield return pos;
				
				foreach (var dirrection in Vector2i.Dirrections)
				{
					var curPos = pos + dirrection;
					
					while (IsPointOnCircuit(curPos)
					       && SignalGrid[curPos.X, curPos.Y].Type == WireType.CrossBrige) curPos += dirrection;
					
					if(!IsPointOnCircuit(curPos) || SignalGrid[curPos.X, curPos.Y].Type != WireType.Normal ||
					   GetNetworkId(curPos) == ignoreId || visited.Contains(curPos))
						continue;
					
					queue.Enqueue(curPos);
				}
			}
		}

		public void SetNetwork(Vector2i start, short networkId, bool stopIfAlreadySet = false)
		{
			if(!IsPointOnCircuit(start)
			   || SignalGrid[start.X, start.Y].Type != WireType.Normal
			   || networkId == -1)
				return;

			if (stopIfAlreadySet)
				SetNetworkId(start, networkId);
			foreach (var wire in EnumerateNetwork(start, stopIfAlreadySet ? networkId : -1))
			{
				if(GetNetworkId(wire) != networkId)
				{
					if(GetNetworkId(wire) != -1)
						Networks[GetNetworkId(wire)] = null;
					SetNetworkId(wire, networkId);
				}
			}
		}

		public void SplitNetwork(Vector2i oldNetworkPoint, Vector2i newNetworkPoint)
		{
			if(!IsPointOnCircuit(oldNetworkPoint) || !IsPointOnCircuit(newNetworkPoint) ||
			   
			   SignalGrid[oldNetworkPoint.X, oldNetworkPoint.Y].Type != WireType.Normal ||
			   SignalGrid[newNetworkPoint.X, newNetworkPoint.Y].Type != WireType.Normal ||
			   
			   GetNetworkId(oldNetworkPoint) != GetNetworkId(newNetworkPoint))
				return;

			short oldId = GetNetworkId(oldNetworkPoint);
			short newId = -1;
			for (short i = 0; i < Networks.Length; i++)
			{
				if(Networks[i] != null) continue;
				newId = i;
				break;
			}
			if (newId == -1) return;

			if (EnumerateNetwork(newNetworkPoint).Contains(oldNetworkPoint)) return;

			foreach (var point in EnumerateNetwork(newNetworkPoint))
				SetNetworkId(point, newId);

			Networks[newId] = new WireNetwork();
		}

		public Vector2i SkipBriges(Vector2i start, Vector2i dirrection)
		{
			while (IsPointOnCircuit(start + dirrection) && SignalGrid[start.X, start.Y].Type == WireType.CrossBrige)
				start += dirrection;
			return start;
		}

		public bool IsPointOnCircuit(Vector2i point)
			=> point.X >= 0 && point.Y >= 0 && point.X < Size.X && point.Y < Size.Y;

		public static CircuitPlane Load(BinaryReader reader)
		{
			var size = new Vector2i(reader.ReadInt32(), reader.ReadInt32());
			var result = new CircuitPlane(size);
			for (var x = 0; x < size.X; x++)
			for (var y = 0; y < size.Y; y++)
			{
				result.Blocks[x, y] = (Blocks) reader.ReadByte();
				var wireType = result.SignalGrid[x, y].Type = (WireType) reader.ReadByte();
				var nid = result.SignalGrid[x, y].NetworkId = reader.ReadInt16();
				if(wireType == WireType.Normal)
					result.Networks[nid] ??= new WireNetwork();
			}
		
			foreach (var network in result.Networks.Where(n => n != null))
			{
				network.CurrentSignal = reader.ReadByte();
				network.RequestedSignal = reader.ReadByte();
			}
		
			for (var i = 0; i < result.Elements.Length; i++) result.Elements[i] = ElementOnCircuitPlane.Load(reader);

			return result;
		}
		
		public void Save(BinaryWriter writer)
		{
			writer.Write(Size.X);
			writer.Write(Size.Y);
			
			for (var x = 0; x < Size.X; x++)
			for (var y = 0; y < Size.Y; y++)
			{
				writer.Write((byte) Blocks[x, y]);
				writer.Write((byte) SignalGrid[x, y].Type);
				writer.Write(SignalGrid[x, y].NetworkId);
			}
		
			foreach (var network in Networks.Where(n => n != null))
			{
				writer.Write(network.CurrentSignal);
				writer.Write(network.RequestedSignal);
			}
		
			foreach (var element in Elements) element.Save(writer);
		}
	}

	public enum WireType : byte
	{
		Empty,
		Normal,
		CrossBrige
	}
	
	public struct WiringData
	{
		public WireType Type;
		public short NetworkId;
	}

	public class WireNetwork
	{
		public byte CurrentSignal, RequestedSignal;

		public WireNetwork(byte currentSignal = 0, byte requestedSignal = 0)
		{
			this.CurrentSignal = currentSignal;
			this.RequestedSignal = requestedSignal;
		}
		
		public byte Signal
		{
			get => CurrentSignal;
			set => RequestedSignal = value > RequestedSignal ? value : RequestedSignal;
		}

		public void RequestSignal(byte signal)
			=> RequestedSignal = signal > RequestedSignal ? signal : RequestedSignal;

		public void SetCurrentSignal()
		{
			CurrentSignal = RequestedSignal;
			RequestedSignal = 0;
		}
	}
}