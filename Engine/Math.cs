﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace OrbitalShipHacker.Engine
{
	public struct Vector2
	{
		public double X, Y;
		public double SqrMagnitude => X * X + Y * Y;
		public double Magnitude => Math.Sqrt(SqrMagnitude);
		public Vector2 Normalized => SqrMagnitude != 0 ? this / Magnitude : Zero;

		public static readonly Vector2 Zero = new Vector2 {X = 0, Y = 0};
		public static readonly Vector2 Up = new Vector2 {X = 0, Y = -1};
		public static readonly Vector2 Down = new Vector2 {X = 0, Y = 1};
		public static readonly Vector2 Left = new Vector2 {X = -1, Y = 0};
		public static readonly Vector2 Right = new Vector2 {X = 1, Y = 0};

		public static Vector2 operator +(Vector2 left, Vector2 right)
			=> new Vector2 {X = left.X + right.X, Y = left.Y + right.Y};
		
		public static Vector2 operator -(Vector2 left, Vector2 right)
			=> new Vector2 {X = left.X - right.X, Y = left.Y - right.Y};
		
		public static Vector2 operator *(Vector2 vector, double number)
			=> new Vector2 {X = vector.X * number, Y = vector.Y * number};

		public static Vector2 operator *(double number, Vector2 vector)
			=> new Vector2 {X = vector.X * number, Y = vector.Y * number};
		
		public static Vector2 operator /(Vector2 vector, double number)
			=> new Vector2 {X = vector.X / number, Y = vector.Y / number};

		public static double operator *(Vector2 left, Vector2 right)
			=> left.X * right.X + left.Y * right.Y;

		public Vector2 Project(Vector2 to)
			=> to.Normalized * this * to.Normalized;
	}

	public struct Vector2i
	{
		public int X, Y;

		public Vector2i(int x, int y)
		{
			X = x;
			Y = y;
		}
		
		public static readonly Vector2i Zero = new Vector2i {X = 0, Y = 0};
		public static readonly Vector2i Up = new Vector2i {X = 0, Y = -1};
		public static readonly Vector2i Down = new Vector2i {X = 0, Y = 1};
		public static readonly Vector2i Left = new Vector2i {X = -1, Y = 0};
		public static readonly Vector2i Right = new Vector2i {X = 1, Y = 0};

		public static readonly Vector2i[] Dirrections = {Right, Up, Left, Down};

		public static implicit operator Vector2(Vector2i vector) => new Vector2 {X = vector.X, Y = vector.Y};
		public static implicit operator Point(Vector2i vector) => new Point {X = vector.X, Y = vector.Y};
		public static implicit operator Size(Vector2i vector) => new Size {Width = vector.X, Height = vector.Y};
		public static implicit operator Vector2i(Point vector) => new Vector2i {X = vector.X, Y = vector.Y};

		public static bool operator ==(Vector2i first, Vector2i second)
			=> first.X == second.X && first.Y == second.Y;

		public static bool operator !=(Vector2i first, Vector2i second) => !(first == second);

		public static Vector2i operator +(Vector2i left, Vector2i right)
			=> new Vector2i {X = left.X + right.X, Y = left.Y + right.Y};
		
		public static Vector2i operator -(Vector2i left, Vector2i right)
			=> new Vector2i {X = left.X - right.X, Y = left.Y - right.Y};
		
		public static Vector2i operator *(Vector2i vector, int number)
			=> new Vector2i {X = vector.X * number, Y = vector.Y * number};

		public static Vector2i operator *(int number, Vector2i vector)
			=> new Vector2i {X = vector.X * number, Y = vector.Y * number};

		public static Vector2i operator -(Vector2i vector) => -1 * vector;
		
		public static Vector2i operator /(Vector2i vector, int number)
			=> new Vector2i {X = vector.X / number, Y = vector.Y / number};

		public static Vector2i operator /(int number, Vector2i vector)
			=> new Vector2i {X = vector.X / number, Y = vector.Y / number};

		public override string ToString() => $"({X}, {Y})";

		// public bool IsInRectangle(Rectangle rectangle)
		// 	=> X >= rectangle.Left && X <= rectangle.Right && Y >= rectangle.Top && Y <= rectangle.Bottom;

		public IEnumerable<Vector2i> GetNeighbours()
		{
			yield return this + new Vector2i(1, 0);
			yield return this + new Vector2i(0, 1);
			yield return this + new Vector2i(-1, 0);
			yield return this + new Vector2i(0, -1);
		}
	}

	public static class VectorExtensions
	{
		public static IEnumerable<Vector2i> LineTo(this Vector2i first, Vector2i second)
		{
			var delta = second - first;
			var k = delta.Y / (double) delta.X;
			var b = first.Y;
			var y = b + (delta.Y >= 0 ? 1 : -1);
			for (var x = 0; (delta.X >= 0) ? (x <= delta.X) : (x >= delta.X); x += (delta.X >= 0) ? 1 : -1)
			for (y -= (delta.Y >= 0) ? 1 : -1;
				(delta.Y >= 0)
					? (y <= (!double.IsInfinity(k) ? (int) (k * x + b) : second.Y))
					: (y >= (!double.IsInfinity(k) ? (int) (k * x + b) : second.Y));
				y += (delta.Y >= 0) ? 1 : -1)
				yield return new Vector2i(first.X + x, y);
		}
	}
}