﻿using System;
using System.Collections;
using System.Drawing;
using System.IO;

namespace OrbitalShipHacker.Engine
{
	public class Level
	{
		public Player Player;
		public CircuitPlane Board;
		public BitArray DisallowedElements;
		public Rectangle AllowedArea;
		public string Description;
		public event Action Passed;

		public Level(Player player, CircuitPlane board, string description = "Enter description here!")
		{
			Player = player;
			Board = board;
			Description = description;
			
			DisallowedElements = new BitArray(board.Elements.Length);
		}

		public void Save(BinaryWriter writer)
		{
			writer.Write(Description);
			
			writer.Write(AllowedArea.X); writer.Write(AllowedArea.Y);
			writer.Write(AllowedArea.Width); writer.Write(AllowedArea.Height);
			
			var array = new byte[DisallowedElements.Length /8];
			DisallowedElements.CopyTo(array, 0);
			writer.Write(array.Length);
			writer.Write(array);
			
			Player.Save(writer);
			Board.Save(writer);
		}

		public static Level Load(BinaryReader reader)
		{
			var desc = reader.ReadString();
			
			var allowedArea = new Rectangle(
				reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32(),reader.ReadInt32());
			var de = new BitArray(reader.ReadBytes(reader.ReadInt32()));

			var result = new Level(
				Player.Load(reader),
				CircuitPlane.Load(reader),
				desc
			);
			foreach (var element in result.Board.Elements)
				if (element.Element is LevelTarget target)
					target.Level = result;
			result.AllowedArea = allowedArea;
			result.DisallowedElements = de;
			return result;
		}

		public void Pass() => Passed();
	}
}