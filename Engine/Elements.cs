﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using OrbitalShipHacker.VM;

namespace OrbitalShipHacker.Engine
{
	public enum Rotations : byte
	{
		Up,
		Right,
		Down,
		Left
	}
	
	public interface ICircuitElement
	{
		Vector2i Size { get; }

		void AcceptInputs(CircuitPlane circuit, Vector2i position, Rotations rotation);
		void SendOutputs(CircuitPlane circuit, Vector2i position, Rotations rotation);
		IEnumerable<Vector2i> ConnectionLocalPoints { get; }
		IEnumerable<Vector2i> OccupiingLocalPoints { get; }
		bool IsOccupiing(Vector2i localPoint);
		void Tick(CircuitPlane circuit, Vector2i position, Rotations rotation);
		ICircuitElement Copy();
	}

	public enum Elements : byte
	{
		Null = 0,
		Mca = 1,
		MagicClock = 2,
		Indicator = 3,
		LevelTarget = 4
	}

	public static class CircuitElementExtensions
	{
		public static ICircuitElement Load(BinaryReader reader) => (Elements) reader.ReadByte() switch
		{
			Elements.Mca => MCA.Load(reader),
			Elements.MagicClock => MagicClock.Load(reader),
			Elements.Indicator => new Indicator(),
			Elements.LevelTarget => new LevelTarget(),
			_ => null
		};

		public static void Save(this ICircuitElement element, BinaryWriter writer)
		{
			switch (element)
			{
				case null:
					writer.Write((byte) Elements.Null);
					return;
				case MCA mca:
					writer.Write((byte) Elements.Mca);
					mca.Save(writer);
					return;
				case MagicClock clock:
					writer.Write((byte) Elements.MagicClock);
					clock.Save(writer);
					return;
				case Indicator indicator:
					writer.Write((byte) Elements.Indicator);
					return;
				case LevelTarget checker:
					writer.Write((byte) Elements.LevelTarget);
					return;
				default: return;
			}
		}

		public static Vector2i LocalToWorld(this ICircuitElement element, Vector2i point, Vector2i position, Rotations rotation)
			=> position + element.RotateLocalPoint(point, rotation);
		public static Vector2i WorldToLocal(this ICircuitElement element, Vector2i point, Vector2i position, Rotations rotation)
			=> element.ReverseRotateLocalPoint(point - position, rotation);
		
		public static Vector2i RotateLocalPoint(this ICircuitElement element, Vector2i localPoint, Rotations rotation) =>
			rotation switch
			{
				Rotations.Up => localPoint,
				Rotations.Right => new Vector2i(element.Size.Y - localPoint.Y - 1, localPoint.X),
				Rotations.Down => new Vector2i(element.Size.X - localPoint.X - 1, element.Size.Y - localPoint.Y - 1),
				Rotations.Left => new Vector2i(localPoint.Y, localPoint.X),
				_ => localPoint
			};

		public static Vector2i ReverseRotateLocalPoint(this ICircuitElement element, Vector2i localPoint, Rotations rotation) =>
			rotation switch
			{
				Rotations.Up => localPoint,
				Rotations.Right => new Vector2i(element.Size.X - localPoint.Y - 1, element.Size.Y - localPoint.X - 1),
				Rotations.Down => new Vector2i(element.Size.X - localPoint.X - 1, element.Size.Y - localPoint.Y - 1),
				Rotations.Left => new Vector2i(element.Size.X - localPoint.Y - 1, localPoint.X),
				_ => localPoint
			};

		public static bool IsFitOnCircuit(this ICircuitElement element, CircuitPlane circuitBoard, Vector2i position, Rotations rotation)
		{
			if (!circuitBoard.IsPointOnCircuit(position)) return false;
			var size = element.Size;
			if (rotation == Rotations.Left || rotation == Rotations.Right)
				(size.X, size.Y) = (size.Y, size.X);
			return circuitBoard.IsPointOnCircuit(position + size - new Vector2i(1, 1));
		}
	}
}